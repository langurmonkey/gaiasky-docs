.. _tutorials:

Additional resources
********************

This page gathers a list of learning resources about Gaia Sky. Find them below int the video tutorials and workshops sections.

Video tutorials
~~~~~~~~~~~~~~~
*  `Gaia Sky video tutorials <https://odysee.com/@GaiaSky:8/Gaia-Sky-tutorials:b>`_
*  `Gaia Sky videos channel <https://odysee.com/@GaiaSky>`_

Presentations
~~~~~~~~~~~~~
*  `Gaia Sky VR <https://gaia.ari.uni-heidelberg.de/gaiasky/presentation/202312/>`_ -- AR/VR for Space Programmes, ESA/ESTEC, Noordwijk, The Netherlands. 2023.

Workshop notes
~~~~~~~~~~~~~~

.. toctree::
   :maxdepth: 1
   
   MWGaiaDN outreach 2024<workshops/mwgaiadn-plntleiden-2024>
   DPAC scripting 2023<workshops/dpac-plenary-hdb-2023>
   DPAC general 2021<workshops/dpac-plenary-online-2021>
   DPAC video 2020<workshops/dpac-plenary-hdb-2020>
    

