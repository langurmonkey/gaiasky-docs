Advanced topics
***************

Below are some chapters which include in-depth information about some of the internal workings of Gaia Sky. Things like the maximum allocated heap memory, the data format or the internal reference system are covered here.

.. toctree::
   :maxdepth: 3
   
   Config-file
   Config-proxy
   Performance
   Graphics-performance
   Internal-reference-system
   Data-format
   STIL-loader
   LOD-catalogs
   Particle-catalogs
   _generated/Archetypes
   _generated/Components
   Star-rendering
   Extrasolar-system
   Cubemaps
   Virtual-textures
   Mesh-warping
