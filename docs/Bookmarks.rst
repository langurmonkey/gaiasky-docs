.. _bookmarks:

.. |bookmark| image:: img/ui/iconic-bookmark.png
   :class: filter-invert

.. raw:: html

   <div class="new new-prev">Since 2.2.6</div>

Bookmarks
*********

Gaia Sky offers a bookmarks manager to keep your favorite objects and locations organized, in the form of the **bookmarks pane**. Open the bookmarks pane by clicking on the bookmark |bookmark| button (to the top-left of the main window), or by pressing :guilabel:`b`.

Bookmarks are laid out in a folder tree. Bookmarks can either be in the root level or in any of the folders, which can also be nested.

There are two types of bookmarks:

- **Object bookmarks** -- the bookmark contains an object, addressed by its name or identifier. When an object bookmark is activated, the camera is put in focus mode and the object becomes the current active focus. If the object does not exist in the current scene, nothing happens. If the object exists but is not visible, a small text appears below the bookmarks tree notifying the user.

.. raw:: html

   <div class="new">Since 3.5.0</div>

- **Location bookmarks** -- the bookmark contains a camera state (position, direction and up vectors) plus time. When the bookmark is activated, the camera is put in free mode and in the state defined by the bookmark. The time is also changed to the time defined in the bookmark.

.. |star| image:: img/ui/iconic-star.png
   :class: filter-invert

.. figure:: img/ui/pane-bookmarks.jpg
  :alt: Bookmarks pane.
  :align: center

  The bookmarks pane in Gaia Sky.

New bookmarks are added at the end of the root level (top of the folder structure). Move bookmarks
around with the context menu that pops up when right clicking on them. This context menu also
provides controls to create new folders and to delete bookmarks. Bookmarks can also be deleted by
clicking on the star next to the name. Once the bookmark is removed, the star's color changes to gray.

Bookmarks are saved to the file ``$GS_CONFIG/bookmarks/bookmarks.txt`` (see :ref:`the folders section <folders>`). The format of the file is straightforward: each non-blank and non-commented (preceded by ``#``) line contains a bookmark. The form of the bookmark is ``folder1/folder2/[...]/$OBJECT``, where ``$OBJECT`` depends on the type of bookmark.

- For **object bookmarks**, ``$OBJECT`` is just the name or identifier:
  
.. code:: text

    Solar System/Moons/Phobos

- For **location bookmarks** ``$OBJECT`` takes the form ``{[x,y,z]|[dx,dy,dz]|[ux,uy,uz]|time_instant|name}`` where:
   
  - ``[x,y,z]`` is the position in the internal reference system and internal units.
  - ``[dx,dy,dz]`` is the camera direction vector, normalized.
  - ``[ux,uy,uz]`` is the camera up vector, normalized.
  - ``time_instant`` is the time, with year, month, day, hour, minute, second and millisecond, in the format 1970-01-01T00:00:00Z.
  - ``name`` is a user-given name to identify the bookmark. Names do not need to be unique, but it is recommended.


You can edit this file directly or share it with others.

This is a valid bookmarks file, containing both object and location bookmarks:


.. code:: text

    # Bookmarks file for Gaia Sky, one bookmark per line, folder separator: '/', comments: '#'
    Stars/Sirius
    Stars/Betelgeuse
    Star Clusters/Pleiades
    Star Clusters/Hyades
    Satellites/Gaia
    Solar System/Sun
    Solar System/Earth
    Solar System/Mercury
    Solar System/Venus
    Solar System/Mars
    Solar System/Phobos
    Solar System/Deimos
    Solar System/Jupiter
    Solar System/Saturn
    Solar System/Uranus
    Solar System/Neptune
    Solar System/Moons/Moon
    Solar System/Moons/Phobos
    Solar System/Moons/Deimos
    Solar System/Moons/Amalthea
    Solar System/Moons/Io
    Solar System/Moons/Europa
    Solar System/Moons/Ganymede
    Solar System/Moons/Callisto
    Solar System/Moons/Prometheus
    Solar System/Moons/Titan
    Solar System/Moons/Rhea
    Solar System/Moons/Dione
    Solar System/Moons/Tethys
    Solar System/Moons/Enceladus
    Solar System/Moons/Mimas
    Solar System/Moons/Janus
    Eclipses/{[-1.3818553459726281232945836836106e2,-5.991742570017757357152905806825e1,2.130396109724412378005830455979e1]|[-0.9548201218738775,0.050259057590566286,-0.29290367357694286]|[0.20409057609035922,0.8273195986777884,-0.5233443592843308]|1601-06-30T02:22:39Z|1601 June 30}
    Eclipses/{[1.1368509657421360252389426851098e2,4.930241284313914004063498650795e1,8.04234541871001128385385982754e1]|[0.6572659958889423,-0.5568060024828526,0.5079059817005352]|[0.452255658439425,0.8304891688337087,0.3251961867233694]|1816-11-19T09:48:15.369Z|1816 November 19}
    Eclipses/{[2.71292992133681124959295785023e1,1.177596714896257159441386475448e1,-1.4555234726955511211277605134986e2]|[0.4097340656192956,-0.6784083087277446,-0.6098197783937811]|[-0.35381119523816085,0.497988712246363,-0.7917227296215221]|1997-03-09T01:13:10.032Z|1997 March 9}


Creating bookmarks
------------------

You can create **object bookmarks** by simply clicking on
the star |star| next to the object's name when in focus. Once the object is in the bookmarks,
the star will brighten up with a clear white color (depending on the UI theme). Object bookmarks
can also be created by right-clicking on the object and selecting |star| :guilabel:`Bookmark: [object name]` in the
context menu that pops up.

You can create **location bookmarks** by positioning the camera in the location, orientation and time of 
your desired bookmark, right clicking anywhere on the scene and selecting |star| :guilabel:`Bookmark current position/time`.

.. figure:: img/screenshots/bookmarks.jpg
  :alt: The bookmarks entries in the context menu, side-by-side to the bookmarks pane.
  :align: center

  The bookmarks entries in the context menu to create an object and a location bookmark.
