.. _mesh-warping:

Mesh warping
************

.. raw:: html

   <div class="new">Since 3.5.0</div>


It is possible to apply an arbitrary warping mesh to distort the final image using a **PFM** (portable float map) file. 

The file format is rather simple, and is `described here <https://netpbm.sourceforge.net/doc/pfm.html>`__. The file contains an array of NxM 3-component pixels in RGB (grayscale not supported). Each position contains the mapped resulting location in UV, in the R and G components respectively, in :math:`[0,1]`. The geometry warp format is the same as in the MPCDI v2.0 specification, section 3.6.2 (`see here <https://vesa.org/vesa-standards/>`__).

This file is read and converted into a mesh by Gaia Sky. The mesh is used to distort the final image at the end of the rendering pipeline.

In order to specify a PFM warping mesh file, you need to edit the :ref:`configuration file <configuration-file>` of Gaia Sky and add a few lines in the ``postprocess`` section:


.. code:: yaml

    postprocess:
      [...]
      warpingMesh:
        pfmFile: /path/to/your/warping-mesh.pfm


A few warping mesh examples (big endian) are provided below:

-  **Identity** --  :download:`warp-identity.pfm <_static/pfm/warp-identity.pfm>` -- identity function, :math:`x'=x, y'=y`.
-  **Flip X** --  :download:`warp-invert-x.pfm <_static/pfm/warp-invert-x.pfm>` -- flips the X coordinate, :math:`x'=1-x, y'=y`.

-  **Flip Y** --  :download:`warp-invert-y.pfm <_static/pfm/warp-invert-y.pfm>` -- flips the Y coordinate, :math:`x'=x, y'=1-y`.

-  **Flip XY** --  :download:`warp-invert-xy.pfm <_static/pfm/warp-invert-xy.pfm>` -- flips the X and Y coordinates, :math:`x'=1-x, y'=1-y`.

-  **X^2** --  :download:`warp-x2.pfm <_static/pfm/warp-x2.pfm>` -- applies a square function to X, :math:`x'=x^2, y'=y`.

-  **X^2, Y^2** --  :download:`warp-x2y2.pfm <_static/pfm/warp-x2y2.pfm>` -- applies a square function to X and Y, :math:`x'=x^2, y'=y^2`.




