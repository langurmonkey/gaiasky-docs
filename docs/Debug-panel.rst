
System information
******************

Gaia Sky has a couple of built-in methods to get information on the system and graphics memory, the frame rate, the graphics device, the LOD status and much more. First, the :ref:`system information panel <debug-panel>` offers a quick and easy way to access all sorts of system information while running Gaia Sky, in the main user interface. Second, the :ref:`debug mode <debug-mode>` enables the logging of additional information to the :ref:`system log <log>`, which can be helpful to analyze crashes or bugs.

.. contents::
  :backlinks: none

.. _debug-panel:

System information panel
------------------------

.. |terminal| image:: img/ui/iconic-terminal.png
   :class: filter-invert

Gaia Sky has a built-in debug information panel that provides a lot of good information on the current system and is hidden by default. You can bring it up with :guilabel:`ctrl` + :guilabel:`d`, or by ticking the :guilabel:`Show debug info` check box in the |terminal| :guilabel:`System` tab of the preferences dialog. 

By default, the system information panel is collapsed.

.. figure:: img/debug/debug-collapsed.jpg
   :alt: Collapsed system information panel.
   :align: center

   Collapsed system information panel, showing the current frame rate (green) and the frame time (white). The small :guilabel:`[+]` icon to the bottom expands the panel.

You can expand it with the :guilabel:`[+]` symbol to get additional information.

.. figure:: img/debug/debug-expanded.jpg
   :alt: Expanded debug panel
   :align: center

   Expanded system information panel, displaying the graphics device, system memory, graphics memory, loaded objects, LOD nodes and SAMP status, additionally to the frame rate and time.

As you can see, it contains information on the current graphics device, system and graphics memory, the amount of objects loaded and on display, the octree (if a LOD dataset is in use) or the SAMP status.

Additional debug information can be obtained in the system tab of the help dialog (:guilabel:`?` or :guilabel:`h`).


.. _debug-mode:

Debug mode
----------

Gaia Sky includes a mode where more information is printed in the standard output (and the log files) to help locate and identify possible problems. This is called **debug mode**. 

In order to run Gaia Sky in debug mode, you need to launch it from the command line (your terminal application of choice in Linux or macOS, PowerShell or cmd in Windows) using the ``-d`` or ``--debug`` flags. 

On **Linux** or **macOS**, fire up your terminal, navigate to your Gaia Sky installation directory, and run:

.. code:: bash

   ./gaiasky --debug

On **Windows**, open PowerShell, navigate to your Gaia Sky installation directory, and run:

.. code:: PowerShell

   .\gaiasky.exe --debug

You will be able to see the log printed out in the terminal window. You can also recover the log files if you need to. More info in the :ref:`logs section <logs>`.
