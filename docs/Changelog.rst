Changelog
*********

*  `Comprehensive version history <https://codeberg.org/gaiasky/gaiasky/releases>`_
*  `Detailed changelog <https://codeberg.org/gaiasky/gaiasky/src/branch/master/CHANGELOG.md>`_
*  `Full commit history <https://codeberg.org/gaiasky/gaiasky/commits/branch/master>`_
