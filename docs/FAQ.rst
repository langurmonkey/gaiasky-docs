.. _faq:

Frequently Asked Questions
**************************

Q: What is the base-data package?
=================================

The `Base data package` is required for Gaia Sky to run and contains basically the Solar System data (textures, models, orbits and attitudes of planets, moons, satellites, etc.). You can't run Gaia Sky without the `base data package`. 

Q: Why do you have two different download pages?
================================================

We list the most important downloads in the official webpage of Gaia Sky (`here <https://gaiasky.space/downloads/>`__) for convenience. The server listing (`here <https://releases.gaiasky.space>`__) provides access to current and old releases.

At the end of the day, if you use the download manager of Gaia Sky, you will never see any of these. If you want to download the data manually, you can do so using either page.

Q: Why so many Gaia-DR catalogs?
================================

We offer several different catalogs based on the latest Gaia data release. **Only one** should be used at a time, as they are different subsets of the same data, meaning that smaller catalogs are contained in larger catalogs. For example, the stars in ``edr3-default`` are contained in ``edr3-large``. We offer so many to give the opportunity to explore the Gaia data to everyone. Even if you have a low-end PC, or don't have lots of disk space to spare, you can still run Gaia Sky with the smaller subsets, which only contain the best stars in terms of parallax relative error. If you have a more capable machine, you can explore larger and larger slices and get more stars in.

Q: Gaia Sky crashes at start-up, what to do?
============================================

First, make sure that your drivers are up to date and your graphics card supports OpenGL ``3.2`` and GLSL ``3.3``.

Some startup crashes are due to inconsistencies in the data. Usually, removing the data folder (``~/.local/share/gaiasky/data`` on Linux, ``%userprofile%\.gaiasky\data`` on Windows, ``~/.gaiasky/data`` on macOS) solves the problem. When Gaia Sky starts again, you will need to re-download the base data pack and the datasets. 

**Debug mode**

You can activate :ref:`debug mode <debug-mode>` to force Gaia Sky to print out much more information, which may help in pinpointing what is going wrong. To do so, you need to launch Gaia Sky from the command line (PowerShell or cmd on Windows) using the `-d` flag.

.. code:: console

    $  gaiasky -d

    
**Configuration file**

Sometimes, the configuration file may get corrupted. To fix this, remove it (``~/.config/gaiasky/config.yaml`` on Linux, ``%userprofile%\.gaiasky\config.yaml`` on Windows, ``~/.gaiasky/config.yaml`` on macOS) and start Gaia Sky again. The default configuration file will be copied to that location and used.

**Getting a crash log**

For modern Gaia Sky versions (> 2.2.0), you can :ref:`find the logs in this location <logs>`.

For old Gaia Sky versions (< 2.2.0), you may need to run Gaia Sky from a terminal. In this case, the procedure depends on your Operating System.

On **Linux**, just run Gaia Sky from the command line and copy the log.


On **Windows**, files named ``output.log`` and ``error.log`` should be created in the installation folder of Gaia Sky. Check if they exist and, if so, attach them to the bug report. Otherwise, just open Power Shell, navigate to the installation folder and run the ``gaiasky.cmd`` script. The log will be printed in the Power Shell window.

On **macOS**, open a Terminal window and write this:

.. code:: console
    
    $  cd /Applications/Gaia\ Sky.app/Contents/Resources/app
    $  chmod u+x ./gaiasky
    $  ./gaiasky

This will launch Gaia Sky in the terminal. Copy the log and paste it in the bug report. `Here is a video <https://youtu.be/B1wbzN-Zk_k>`__ demonstrating how to do this on macOS.

Once you have a log, create a bug report `here <https://codeberg.org/gaiasky/gaiasky/issues>`__, attach the log, and we'll get to it ASAP.

Q: I'm running out of memory, what to do?
=========================================

Don't fret. Check out the :ref:`maximum heap space section <heap-memory>` to learn how to increase the maximum heap memory allocated to Gaia Sky.
If you computer does not have enough physical RAM, try using a smaller dataset.

Q: I can't see the elevation data on Earth or other planets!
============================================================

First, make sure you are using at least version `2.2.0`. Then, make sure that your graphics card supports tessellation (OpenGL 4.x). Then, download the High-resolution texture pack using the download manager and select ``High`` or ``Ultra`` in graphics quality. This is not strictly necessary, but it is much better to use higher resolution data if possible. Finally, select `Tessellation` in the "Elevation representation" drop-down of the graphics pane in the settings window. See the :ref:`elevation (height) section <elevation-height>`.

Q: What is the internal reference system used in Gaia Sky?
==========================================================

The reference system is described in :ref:`reference-system`. The internal workings of Gaia Sky are described in `this paper <https://vcg.iwr.uni-heidelberg.de/publications/pubdetails/Sagrista2019GaiaSky/>`__.

Q: Can I contribute?
====================

Yes. You can contribute translations (currently EN, DE, CA, FR, SK, ES and BG are available) or code. Please, have a look at the `contributing guidelines <https://codeberg.org/gaiasky/gaiasky/src/branch/master/CONTRIBUTING.md>`__.

Q: I like Gaia Sky so much, can I donate to contribute to the project?
======================================================================

Thanks a lot, but no. You may donate to any other awesome open source project of your choosing instead.
