.. _user-interface:
.. _control-panes:

.. role:: green

.. |browser| image:: img/ui/iconic-browser.png
   :class: filter-invert
.. |terminal| image:: img/ui/iconic-terminal.png
   :class: filter-invert

User interface
**************

   .. note:: Since Gaia Sky 3.5.5, Gaia Sky offers two UI modes: the **new UI** and the **old control panel**.

The main elements in the user interface are the control panes, the camera info panel, the quick info bar, the action buttons and the system info panel. They are all described in this section.

.. contents::
  :backlinks: none

Control panes
=============

The most important actions in Gaia Sky can be accessed via the control panes, anchored to the top-left of the screen. There are seven panes, `Time <#time>`__,
`Camera <#camera>`__, `Type visibility <#type-visibility>`__, `Visual settings <#visual-settings>`__, `Datasets <#datasets>`__, `Objects <#objects>`__,  and `Music <#music>`__.

The panes are accessed via the control panel (in the old UI), or via buttons anchored to the left of the screen (new UI).

+----------------------------------------------------+----------------------------------------------------+-------------------------------------------------------+
| .. image:: img/ui/new-ui.jpg                       | .. image:: img/ui/control-panel-collapsed.jpg      | .. image:: img/ui/control-panel-expanded.jpg          |
|   :width: 75%                                      |   :width: 65%                                      |   :width: 65%                                         |
|   :alt: The new UI with its buttons to the left    |   :alt: User interface with all panes collapsed    |   :alt: User interface with camera pane expanded      |
|   :target: ../_images/new-ui.jpg                   |   :target: ../_images/control-panel-collapsed.jpg  |   :target: ../_images/control-panel-expanded.jpg      | 
+----------------------------------------------------+----------------------------------------------------+-------------------------------------------------------+            
| | The new UI, based on anchored                    | | Collapsed control panel                          | | Expanded control panel                              |
| | buttons to the left of the screen.               | | (old UI)                                         | | (old UI)                                            |
+----------------------------------------------------+----------------------------------------------------+-------------------------------------------------------+


The seven panes, except for the Time pane in the old UI, are hidden at startup. To expand them and reveal its controls just click on the little arrow bottom
icon |caret-bottom| at the right of the pane title (in the old UI), or click on the corresponding button (new UI). Use the arrow right icon |caret-right|, or the corresponding button to collapse them again. In the old UI, panes can also be detached
to their own window. To do so, use the detach icon |detach-icon|.

The seven panes are:

- :ref:`time-pane`.
- :ref:`camera-pane`.
- :ref:`visibility-pane`.
- :ref:`visual-settings-pane`.
- :ref:`datasets-pane-stub`.
- :ref:`location-log-pane`.
- :ref:`bookmarks-pane`.

.. |caret-bottom| image:: img/ui/iconic-caret-bottom.png
   :class: filter-invert
.. |caret-right| image:: img/ui/iconic-caret-right.png
   :class: filter-invert
.. |fwd-icon| image:: img/ui/media/iconic-media-skip-forward.png
   :class: filter-invert
.. |bwd-icon| image:: img/ui/media/iconic-media-skip-backward.png
   :class: filter-invert
.. |detach-icon| image:: img/ui/detach-icon.png
   :class: filter-invert

.. |time| image:: img/ui/iconic-clock.png
   :class: filter-invert
.. |camera| image:: img/ui/iconic-video.png
   :class: filter-invert
.. |eye| image:: img/ui/eye-icon.png
   :class: filter-invert
.. |bolt| image:: img/ui/iconic-bolt.png
   :class: filter-invert
.. |datasets| image:: img/ui/iconic-hard-drive.png
   :class: filter-invert
.. |log| image:: img/ui/iconic-map-marker.png
   :class: filter-invert
.. |bookmark| image:: img/ui/iconic-bookmark.png
   :class: filter-invert
   

.. _time-pane:

Time pane
---------
   
.. hint:: Expand and collapse the time pane by clicking on the clock |time| button or with :guilabel:`t`.

Play and pause the simulation using the |play-icon|/|pause-icon| ``Play/Pause`` buttons in the time pane, or toggle using :guilabel:`Space`. You can also change time warp, which is expressed as a scaling factor, using the provided **Warp factor** slider. Use :guilabel:`,` or |bwd-icon| and :guilabel:`.` or |fwd-icon| to divide by 2 and double the value of the time warp respectively. If you keep either of those pressed, the warp factor will increase or decrease steadily.

Use the :guilabel:`Reset time and warp` button to reset the time warp to x1, and set the time to the current real world time (UTC).

.. figure:: img/ui/pane-time.jpg  
  :width: 45%                                  
  :align: center
  :target: ../_images/pane-time.jpg
    
  The time pane displays the simulation date and time, along with controls to start and pause the time, and a slider to control the time warp (speed).


.. |play-icon| image:: img/ui/media/iconic-media-play.png
   :class: filter-invert
.. |pause-icon| image:: img/ui/media/iconic-media-pause.png
   :class: filter-invert
.. |warp-factor| image:: img/ui/pane-time.jpg
   :class: filter-invert

.. _camera-pane:

Camera pane
-----------

.. hint:: Expand and collapse the camera pane by clicking on the camera |camera| button or with :guilabel:`c`.

In the camera options pane on the left you can select the type of
camera. This can also be done by using the :guilabel:`Numpad 0-3` keys.

.. figure:: img/ui/pane-camera.jpg  
  :width: 45%                                  
  :align: center
  :target: ../_images/pane-camera.jpg
    
  The camera pane contains controls related to the camera setup and operation.

There are four camera modes:

* **Free mode** -- the camera is not linked to any object and its velocity is exponential with respect to the distance to the origin (Sun).
* **Focus mode** -- the camera is linked to a focus object and it rotates and rolls with respect to it.
* **Game mode** -- a game mode which maps the controls :guilabel:`wasd` + mouse look.
* **Spacecraft**-- take control of a spacecraft and navigate around at will.

For more information on the camera modes, see the :ref:`camera-modes` section.

Additionally, there are a number of sliders for you to control different
parameters of the camera:

-  **Field of view** -- control the field of view angle of the camera.
   The bigger it is, the larger the portion of the scene represented.
-  **Camera speed** -- control the longitudinal speed of the camera, i.e. how fast it goes forward and backward.
-  **Rotation speed** -- control the transversal speed of the camera, i.e. how
   fast it rotates around an object.
-  **Turn speed** -- control the turning speed of the camera, i.e. how fast it changes its orientation (yaw, pitch and roll).

The checkbox **Cinematic camera** enables the cinematic behavior, described in the :ref:`camera behaviors section <camera-behaviors>`. 

The checkbox **Lock the camera to focus** links the reference system of the camera to that of the focus object and thus it moves with it. When focus lock is checked, the camera stays at the same relative position to the focus object.

The checkbox **Lock orientation** applies the rotation transformation of the focus to the camera, so that the camera rotates when the focus does.

.. _type-visibility:
.. _visibility-pane:

Visibility pane
---------------

.. hint:: Expand and collapse the visibility pane by clicking on the eye |eye| button or with :guilabel:`v`.

The visibility pane offers controls to hide and show object types. Object types are groups of objects that are of the same category, like stars, planets, labels, galaxies, grids, etc. The pane also contains a button at the bottom that gives access to the :ref:`per-object visibility window <individual-visibility>`, which enables visibility control for individual objects.

.. figure:: img/ui/pane-visibility.jpg  
  :width: 45%                                  
  :align: center
  :target: ../_images/pane-visibility.jpg
    
  The visibility pane contains controls to hide and show types of objects.

For example you can hide the stars by clicking on the ``stars`` |stars| toggle. The object types available are the following:

-  |stars| -- Stars
-  |planets| -- Planets
-  |moons| -- Moons
-  |satellites| -- Satellites
-  |asteroids| -- Asteroids
-  |clusters| -- Star clusters
-  |milkyway| -- Milky Way
-  |galaxies| -- Galaxies
-  |nebulae| -- Nebulae
-  |meshes| -- Meshes
-  |equatorial| -- Equatorial grid
-  |ecliptic| -- Ecliptic grid
-  |galactic| -- Galactic grid
-  |recgrid| -- Recursive grid
-  |labels| -- Labels
-  |titles| -- Titles
-  |orbits| -- Orbits
-  |locations| -- Locations
-  |cosmiclocations| -- Cosmic locations
-  |countries| -- Countries
-  |constellations| -- Constellations
-  |boundaries| -- Constellation boundaries
-  |ruler| -- Rulers
-  |effects| -- Particle effects
-  |atmospheres| -- Atmospheres
-  |clouds| -- Clouds
-  |axes| -- Axes
-  |arrows| -- Velocity vectors
-  |keyframes| -- Keyframes
-  |others| -- Others

.. |stars| image:: img/ui/ct/icon-elem-stars.png
   :class: filter-invert
.. |planets| image:: img/ui/ct/icon-elem-planets.png
   :class: filter-invert
.. |moons| image:: img/ui/ct/icon-elem-moons.png
   :class: filter-invert
.. |satellites| image:: img/ui/ct/icon-elem-satellites.png
   :class: filter-invert
.. |asteroids| image:: img/ui/ct/icon-elem-asteroids.png
   :class: filter-invert
.. |clusters| image:: img/ui/ct/icon-elem-clusters.png
   :class: filter-invert
.. |milkyway| image:: img/ui/ct/icon-elem-milkyway.png
   :class: filter-invert
.. |galaxies| image:: img/ui/ct/icon-elem-galaxies.png
   :class: filter-invert
.. |nebulae| image:: img/ui/ct/icon-elem-nebulae.png
   :class: filter-invert
.. |meshes| image:: img/ui/ct/icon-elem-meshes.png
   :class: filter-invert
.. |equatorial| image:: img/ui/ct/icon-elem-equatorial.png
   :class: filter-invert
.. |ecliptic| image:: img/ui/ct/icon-elem-ecliptic.png
   :class: filter-invert
.. |galactic| image:: img/ui/ct/icon-elem-galactic.png
   :class: filter-invert
.. |recgrid| image:: img/ui/ct/icon-elem-recgrid.png
   :class: filter-invert
.. |labels| image:: img/ui/ct/icon-elem-labels.png
   :class: filter-invert
.. |titles| image:: img/ui/ct/icon-elem-titles.png
   :class: filter-invert
.. |orbits| image:: img/ui/ct/icon-elem-orbits.png
   :class: filter-invert
.. |locations| image:: img/ui/ct/icon-elem-locations.png
   :class: filter-invert
.. |cosmiclocations| image:: img/ui/ct/icon-elem-cosmiclocations.png
   :class: filter-invert
.. |countries| image:: img/ui/ct/icon-elem-countries.png
   :class: filter-invert
.. |constellations| image:: img/ui/ct/icon-elem-constellations.png
   :class: filter-invert
.. |boundaries| image:: img/ui/ct/icon-elem-boundaries.png
   :class: filter-invert
.. |ruler| image:: img/ui/ct/icon-elem-ruler.png
   :class: filter-invert
.. |effects| image:: img/ui/ct/icon-elem-effects.png
   :class: filter-invert
.. |atmospheres| image:: img/ui/ct/icon-elem-atmospheres.png
   :class: filter-invert
.. |clouds| image:: img/ui/ct/icon-elem-clouds.png
   :class: filter-invert
.. |axes| image:: img/ui/ct/icon-elem-axes.png
   :class: filter-invert
.. |arrows| image:: img/ui/ct/icon-elem-arrows.png
   :class: filter-invert
.. |keyframes| image:: img/ui/ct/icon-elem-keyframes.png
   :class: filter-invert
.. |others| image:: img/ui/ct/icon-elem-others.png
   :class: filter-invert

.. _individual-visibility:

Per-object visibility
^^^^^^^^^^^^^^^^^^^^^

This button provides access to controls to manipulate the individual visibility of
objects.

.. figure:: img/screenshots/per-object-vis.jpg
  :alt: Individual object visibility
  :align: center
  :target: ../_images/per-object-vis.jpg

  Individual object visibility button and dialog


.. raw:: html

   <div class="new new-prev">Since 3.1.0</div>

As shown in the image above, when clicking the :guilabel:`Per-object visibility` button, a new
dialog appears, from which individual objects can be toggled on and off. They are organized
per object type (top of the dialog). Once the object type is selected, the list of object appears 
in the bottom part.

.. hint:: **Stars do not appear** in the per-object visibility panel!

Since there are so many stars, they are not in the per-object visibility panel as single objects. Instead,
they show up in groups. A single standalone catalog is a single star group. In the case of LOD catalogs like
the ones based on Gaia data releases, each octree node contains a star group. However, individual star
visibility can still be manipulated using the eye icon in the focus information pane when the
star is focused.

.. _velocityvectors:

Velocity vectors
^^^^^^^^^^^^^^^^

Enabling **velocity vectors** activates the
representation of star velocities, if the currently loaded catalog
provides them. Once velocity vectors are activated, a few extra controls become
available to tweak their length and color.

.. figure:: img/screenshots/velocity-vectors.jpg
  :alt: Velocity vectors in Gaia Sky
  :align: center
  :target: ../_images/velocity-vectors.jpg

  Velocity vectors in Gaia Sky

*  **Number factor** -- control how many velocity vectors are rendered. The stars are sorted by magnitude (ascending) so the brightest stars will get velocity vectors first.
*  **Length factor** -- length factor to scale the velocity vectors.
*  **Color mode** -- choose the color scheme for the velocity vectors:
  
    *  **Direction** -- color-code the vectors by their direction. The vectors :math:`\vec{v}` are pre-processed (:math:`\vec{v}'=\frac{|\vec{v}|+1}{2}`) and then the :math:`xyz` components are mapped to the colors :math:`rgb`.
    *  **Speed** -- the speed is normalized in from :math:`[0,100] Km/h` to :math:`[0,1]` and then mapped to colors using a long rainbow colormap (see `here <http://www.particleincell.com/blog/2014/colormap/>`__).
    *  **Has radial velocity** -- stars in blue have radial velocity, stars in red have no radial velocity.
    *  **Redshift from the Sun** -- map the redshift (radial velocity) from the sun using a red-to-blue colormap.
    *  **Redshift from the camera** -- map the redshift (radial velocity) from the current camera position using a red-to-blue colormap.
    *  **Solid color** -- use a solid color for all arrows.

*  **Show arrowheads** -- Whether to show the vectors with arrow caps or not.

.. hint:: Control the width of the velocity vectors with the **line width** slider in the **visual settings** pane.

.. _interface-lighting:
.. _visual-settings:
.. _visual-settings-pane:

Visual settings pane
--------------------

.. hint:: Expand and collapse the visual settings pane by clicking on the bolt |bolt| button or with :guilabel:`l`.

The **visual settings** pane contains a few options to control the shading of stars and other elements:

.. raw:: html

   <div class="new new-prev">Since 3.0.0</div>

-  **Brightness power** -- exponent of power function that controls the brightness of stars. Makes bright stars brighter and faint stars fainter.
-  **Star brightness** -- control the brightness of stars.
-  **Star size (px)** -- control the size of point-like stars.
-  **Min. star opacity** -- set a minimum opacity for the faintest stars.
-  **Ambient light** -- control the amount of ambient light. This only
   affects the models such as the planets or satellites.
-  **Line width** -- control the width of all lines in Gaia Sky (orbits, velocity vectors, etc.).
-  **Label size** -- control the size of the labels.
-  **Elevation multiplier** -- scale the height representation.

.. figure:: img/ui/pane-visual-settings.jpg
  :width: 45%                                  
  :align: center
  :target: ../_images/pane-visual-settings.jpg

  The visual settings pane with all its sliders.

.. _datasets-pane-stub:

Datasets pane
-------------

.. hint:: Expand and collapse the datasets pane by clicking on the hard disk |datasets| button or with :guilabel:`d`.

The **datasets pane** contains all the datasets currently loaded. For each dataset, a highlight color can be defined. The dataset visual settings window can be used to modify the particle aspect, highlighting properties or the transition limits.

.. figure:: img/ui/pane-datasets.jpg
  :width: 45%                                  
  :align: center
  :target: ../_images/pane-datasets.jpg

  The datasets pane with three datasets (Milky Way, Gaia DR3 tiny, BH2 system).

It is also possible to define arbitrary filters on any of the properties of the elements of the dataset, and to add arbitrary affine transformations. Datasets can be highlighted by clicking on the little crosshair below the name.

Please see the :ref:`datasets pane section <datasets-pane>` for more 
information on this.

.. _location-log-pane:

Location log pane
-----------------

.. hint:: Expand and collapse the location log pane by clicking on the map marker |log| button.

Gaia Sky keeps track of the visited locations during a session, up to 200 entries. More information on the location log can be found in the :ref:`location log section <locationlog>`.

.. figure:: img/ui/pane-location-log.jpg
  :width: 45%                                  
  :align: center
  :target: ../_images/pane-location-log.jpg

  The location log pane keeps track of the objects you have visited.

.. _bookmarks-pane:

Bookmarks pane
--------------

.. hint:: Expand and collapse the bookmarks pane by clicking on the bookmark |bookmark| button or with :guilabel:`b`.

Gaia Sky offers a bookmark system to keep your favorite objects
organized and at hand. This panel centralizes the operation of
bookmarks. You can find more information on this in the
:ref:`bookmarks section <bookmarks>`.

.. figure:: img/ui/pane-bookmarks.jpg
  :width: 45%                                  
  :align: center
  :target: ../_images/pane-bookmarks.jpg

  The bookmarks pane shows the user-defined bookmarks.

Camera info panel
=================

The **camera info panel**, also known as **focus info panel**, is anchored to the bottom-right of the main window. See the :ref:`camera info panel section <camera-info-panel>` for more information.

.. figure:: img/ui/camera-info-pane-focusmode.jpg
  :width: 35%                                  
  :align: center
  :target: ../_images/camera-info-pane-focusmode.jpg

  The camera info pane when the camera is in focus mode. In this state, it is also referred to as focus info pane, and it displays information on the focus (top), the mouse pointer (middle), and the camera position and state (bottom).


.. _quick-info-bar:

Quick info bar
==============

Anchored to the top of the screen is the :blue:`quick info bar`, which provides the following information at a glance:

- **Simulation date and time** -- click it to open the date/time picker window to edit the time.
- **Time warp** -- the current speed of time, or "time off" if time is paused.
- :green:`Current focus object` -- the current camera focus object, if any.
- :blue:`Current closest object` -- the current object closest to our location.
- :orange:`Current home object` -- the home object. This is typically the Earth, but can be changed by editing the :ref:`configuration file <configuration-file>`.

The colors of the focus, closest and home objects correspond to the colors of the :ref:`cross-hairs <user-interface-config>`. The cross-hairs can be enabled or disabled from the |browser| :guilabel:`Interface settings` tab in the preferences window (use :guilabel:`p` to bring it up).


.. figure:: img/ui/quick-info-bar.jpg
  :align: center
  :target: ../_images/quick-info-bar.jpg

  The quick info bar is anchored to the top of the window and displays useful information at a glance.

Action buttons
==============

Anchored to the bottom-left are some buttons to perform some special actions. They are described in the following sub-sections:

- :ref:`Minimap <minimap-button>`.
- :ref:`Load dataset <load-dataset-button>`.
- :ref:`Preferences window <preferences-button>`.
- :ref:`System log <system-log-button>`.
- :ref:`About/help <help-button>`.
- :ref:`Exit <exit-button>`.

.. _minimap-button:

Minimap
-------

Use the mini-map |minimap| button or :guilabel:`Tab` to toggle the mini-map on an off. The mini-map offers a contextual view of your position as a top and side projection, relative to the closest objects and the distance to the Sun.

.. |minimap| image:: img/ui/map-icon.png
   :class: filter-invert

.. _load-dataset-button:

Load dataset
------------

Use the open folder |dsload| button or :guilabel:`Ctrl` + :guilabel:`o` to load a new VOTable file (``.vot``) into Gaia Sky. The :ref:`dataset loading section <loading-datasets>` contains more information on dataset loading. Also, check out the :ref:`stil-data-provider` section for more information on the metadata needed for Gaia Sky to parse the dataset correctly.

.. |dsload| image:: img/ui/open-icon.png
   :class: filter-invert

.. _preferences-button:

Preferences window
------------------

Use the preferences |prefsicon| button, or :guilabel:`p`, to bring up the preferences window, from which the settings and configuration can be modified. For a detailed description of the configuration options refer to the :ref:`Configuration Instructions <configuration>`.

.. |prefsicon| image:: img/ui/prefs-icon.png
   :class: filter-invert

.. _system-log-button:

System log
----------

Use the log |logicon| button, or :guilabel:`Alt` + :guilabel:`l`, to bring up the system log window, which displays the Gaia Sky log for the current session. The log can be exported to a file by clicking on the ``Export to
file`` button. The location of the exported log files is ``$GS_DATA`` (see :ref:`folders <folders>`).

.. |logicon| image:: img/ui/log-icon.png
   :class: filter-invert

.. _help-button:

About/help
----------

Use the help |helpicon| button, or :guilabel:`h`, to bring up the help dialog, where information on the current system, OpenGL settings, Java memory, updates and contact can be found.

.. |helpicon| image:: img/ui/help-icon.png
   :class: filter-invert

.. _exit-button:

Exit
----

Click on the cross icon |quiticon| to exit Gaia Sky. You can also use :guilabel:`Esc`.

.. |quiticon| image:: img/ui/quit-icon.png
   :class: filter-invert

System info panel
=================

Bring up the system info panel by hitting :guilabel:`Ctrl` + :guilabel:`d`, or by using the :guilabel:`Show debug info` checkbox in the |terminal| :guilabel:`System` tab in the preferences window. The :ref:`system information panel section <debug-panel>` contains more information on this topic.
