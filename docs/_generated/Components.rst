.. _components:

Components
----------

This section lists all components, together with a description and all the attributes. For each attribute, we provide a description and list its units and possible aliases. We also note the :ref:`archetypes` that have the component.

A general description of archetypes and components is provided in :ref:`data-morphology`.

All components are: :ref:`Base <comp-Base>`, :ref:`Body <comp-Body>`, :ref:`GraphNode <comp-GraphNode>`, :ref:`Coordinates <comp-Coordinates>`, :ref:`Orientation <comp-Orientation>`, :ref:`Celestial <comp-Celestial>`, :ref:`Magnitude <comp-Magnitude>`, :ref:`ProperMotion <comp-ProperMotion>`, :ref:`SolidAngle <comp-SolidAngle>`, :ref:`Shape <comp-Shape>`, :ref:`Trajectory <comp-Trajectory>`, :ref:`ModelScaffolding <comp-ModelScaffolding>`, :ref:`Model <comp-Model>`, :ref:`Volume <comp-Volume>`, :ref:`Atmosphere <comp-Atmosphere>`, :ref:`Cloud <comp-Cloud>`, :ref:`RenderFlags <comp-RenderFlags>`, :ref:`MotorEngine <comp-MotorEngine>`, :ref:`RefSysTransform <comp-RefSysTransform>`, :ref:`AffineTransformations <comp-AffineTransformations>`, :ref:`Fade <comp-Fade>`, :ref:`DatasetDescription <comp-DatasetDescription>`, :ref:`Label <comp-Label>`, :ref:`RenderType <comp-RenderType>`, :ref:`BillboardSet <comp-BillboardSet>`, :ref:`Title <comp-Title>`, :ref:`Axis <comp-Axis>`, :ref:`LocationMark <comp-LocationMark>`, :ref:`Constel <comp-Constel>`, :ref:`Boundaries <comp-Boundaries>`, :ref:`ParticleSet <comp-ParticleSet>`, :ref:`StarSet <comp-StarSet>`, :ref:`ParticleExtra <comp-ParticleExtra>`, :ref:`Mesh <comp-Mesh>`, :ref:`Focus <comp-Focus>`, :ref:`Raymarching <comp-Raymarching>`, :ref:`Highlight <comp-Highlight>`.


.. _comp-Base:

Base
~~~~

Defines basic attributes common to all objects.

This component is in the following archetypes: :ref:`SceneGraphNode <arch-SceneGraphNode>`, :ref:`Universe <arch-Universe>`.

.. list-table:: Base attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``id``
     - The ID of the object, typically set automatically by Gaia Sky.
     - 
   * - ``name``
     - A single object name, used to identify the object and as a label text, if any. If the object already has names, this attribute overrides the first one in the name list. The first name in the list is also used as the i18n key for translations.
     - 
   * - ``names``
     - A list of names, used to identify the object. It overrides the full name list. The first name in the list is used as a label text, and as a i18n key.
     - 
   * - ``altName``
     - Adds a new name to the name list of this object, at the end.
     - ``altname``
   * - ``opacity``
     - Static opacity value. Typically, this gets overwritten internally in the update process.
     - 
   * - ``componentType``
     - The content type string (or list) for this object. Content types control the visibility of objects. Examples of content types are 'Planets', 'Asteroids', 'Stars', 'Labels', etc.
     - ``ct``, ``componentTypes``


.. _comp-Body:

Body
~~~~

Defines physical body attributes common to all objects.

This component is in the following archetypes: :ref:`SceneGraphNode <arch-SceneGraphNode>`, :ref:`Universe <arch-Universe>`.

.. list-table:: Body attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``position``
     - The position of the object. This is the position at epoch if the object has a proper motion, or just a static position. Given in the internal reference system and in internal units by default (see aliases for other units).
     - ``pos``, ``positionKm``, ``posKm``, ``positionPc``, ``posPc``
   * - ``size``
     - The diameter of the entity (in some archetypes this is the radius). The default attribute uses internal units (see aliases for other units).
     - ``sizeKm``, ``sizePc``, ``sizepc``, ``sizeM``, ``sizeAU``, ``diameter``, ``diameterKm``, ``diameterPc``
   * - ``cameraCollision``
     - Enable or disable camera collision with this object's bounding sphere. This means that the camera is not permitted to enter this object's radius.
     - 
   * - ``radius``
     - The half-size. See size attribute.
     - ``radiusKm``, ``radiusPc``
   * - ``color``
     - The color of the entity, as a RGBA quartet. Used as the general color of the entity. The last value in the list, alpha, also acts as a transparency value. The color is also applied to the object label unless 'labelColor' is specified.
     - 
   * - ``labelColor``
     - The color of the label of this entity. If set, the label of this entity uses this color. Otherwise, it uses the global entity color.
     - ``labelcolor``


.. _comp-GraphNode:

GraphNode
~~~~~~~~~

Defines attributes pertaining to the scene graph hierarchy.

This component is in the following archetypes: :ref:`SceneGraphNode <arch-SceneGraphNode>`, :ref:`Universe <arch-Universe>`.

.. list-table:: GraphNode attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``parent``
     - Name of the parent entity in the scene graph. Positions for every object are typically relative to the position of the parent. In some cases, the orientation of the parent is also contemplated.
     - 


.. _comp-Coordinates:

Coordinates
~~~~~~~~~~~

Defines attributes that provide coordinates and positions to objects.

This component is in the following archetypes: :ref:`CelestialBody <arch-CelestialBody>`, :ref:`BackgroundModel <arch-BackgroundModel>`, :ref:`BillboardGroup <arch-BillboardGroup>`, :ref:`Model <arch-Model>`.

.. list-table:: Coordinates attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``coordinatesProvider``
     - The coordinates provider object for this object. The coordinates provider computes the position of the object for each time. This is an object containing, at least, the full reference to a Java class that implements ``IBodyCoordinates`` in the "impl" attribute. Examples are ``gaiasky.util.coord.StaticCoordinates`` or ``gaiasky.util.coord.OrbitLintCoordinates``. See :ref:`coordinate-providers` for more information.
     - ``coordinates``


.. _comp-Orientation:

Orientation
~~~~~~~~~~~

Defines the orientation model of objects. Can be defined as a rigid rotation (given parameters like rotation period, axial tilt, etc.) or via quaternion-based orientations.

This component is in the following archetypes: :ref:`CelestialBody <arch-CelestialBody>`, :ref:`Satellite <arch-Satellite>`.

.. list-table:: Orientation attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``rotation``
     - The rotation object for this object. This attribute describes a rigid body rotation. This is given in the form of a map with the attributes ``angularVelocity``, ``period``, ``axialtilt``, ``inclination``, ``ascendingNode`` and ``meridianAngle``. See :ref:`rotation` for more information.
     - ``rigidRotation``
   * - ``orientationProvider``
     - Provider class for the quaternion orientations.
     - ``provider``, ``attitudeProvider``
   * - ``orientationSource``
     - Location of the data file(s), necessary to initialize the quaternion orientation provider.
     - ``attitudeLocation``


.. _comp-Celestial:

Celestial
~~~~~~~~~

Defines attributes common to all celestial objects (stars, planets, moons, etc.).

This component is in the following archetypes: :ref:`CelestialBody <arch-CelestialBody>`.

.. list-table:: Celestial attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``wikiName``
     - The name to look up this object in the wikipedia, if any. If this is set, a '+ info' button appears in the focus info interface when this object is the focus, enabling the user to pull information on the object directly from Gaia Sky and display it in a window.
     - ``wikiname``
   * - ``colorBV``
     - The color index B-V of this object. This is only ever used in single particles/stars, and when no 'color' attribute has been specified. If that is the case, we convert the B-V index into an RGB color and use it as the object's global color.
     - ``colorbv``, ``colorBv``, ``colorIndex``


.. _comp-Magnitude:

Magnitude
~~~~~~~~~

Defines magnitude attributes, both apparent and absolute.

This component is in the following archetypes: :ref:`CelestialBody <arch-CelestialBody>`.

.. list-table:: Magnitude attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``appMag``
     - The apparent magnitude. If it is not given, it is computed automatically from the absolute magnitude (if present) and the distance.
     - ``appmag``, ``apparentMagnitude``
   * - ``absMag``
     - The absolute magnitude. If it is not given, it is computed automatically from the apparent magnitude (if present) and the distance. In single stars, the absolute magnitude is used to compute the pseudo-size. See the 'star rendering' section for more information.
     - ``absmag``, ``absoluteMagnitude``


.. _comp-ProperMotion:

ProperMotion
~~~~~~~~~~~~

Defines proper motion attributes.

This component is in the following archetypes: :ref:`Particle <arch-Particle>`, :ref:`StarCluster <arch-StarCluster>`.

.. list-table:: ProperMotion attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``muAlpha``
     - Proper motion in right ascension, the :math:`\mu_{\alpha\star}`, in mas/yr.
     - ``muAlphaMasYr``
   * - ``muDelta``
     - Proper motion in declination, the :math:`\mu_{\delta}`, in mas/yr.
     - ``muDeltaMasYr``
   * - ``radialVelocity``
     - The radial velocity, in km/s.
     - ``rv``, ``rvKms``, ``radialVelocityKms``
   * - ``epochJd``
     - The epoch as a Julian date. For instance, 2015.5 corresponds to a Julian date of 2457206.125.
     - 
   * - ``epochYear``
     - The epoch as a year plus fraction (e.g. 2015.5). This gets converted to a Julian date internally.
     - 


.. _comp-SolidAngle:

SolidAngle
~~~~~~~~~~

Defines solid angle thresholds for the various rendering modes.

This component is in the following archetypes: :ref:`CelestialBody <arch-CelestialBody>`, :ref:`StarCluster <arch-StarCluster>`, :ref:`Model <arch-Model>`.

.. list-table:: SolidAngle attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``thresholdNone``
     - Solid angle threshold to start rendering this object at all. Mainly for internal use. Gets overwritten during initialization.
     - 
   * - ``thresholdPoint``
     - Solid angle threshold boundary between rendering the object as a point and as a quad. Mainly for internal use. Gets overwritten during initialization.
     - 
   * - ``thresholdQuad``
     - Solid angle threshold boundary between rendering the object as a quad and as a model. Mainly for internal use. Gets overwritten during initialization.
     - 


.. _comp-Shape:

Shape
~~~~~

Defines attributes related to shape objects

This component is in the following archetypes: :ref:`ShapeObject <arch-ShapeObject>`.

.. list-table:: Shape attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``track``
     - Shape objects can use the position of other objects as their own. This is useful when, for example, we want to add a wireframe sphere around an object. This attribute contains the name of the object whose position we are to track.
     - ``trackName``


.. _comp-Trajectory:

Trajectory
~~~~~~~~~~

Defines attributes related to orbits and trajectory objects. See :ref:`orbits` for more information.

This component is in the following archetypes: :ref:`Orbit <arch-Orbit>`.

.. list-table:: Trajectory attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``orbitProvider``
     - In *Orbit* archetype objects, this is the fully-qualified Java class that provides orbit data. This class needs to implement ``IOrbitDataProvider``. |brsp| **Values:** |br| ``gaiasky.data.orbit.OrbitalParametersProvider`` -- orbit is defined with orbital elements. |br| ``gaiasky.data.orbit.OrbitFileDataProvider`` -- orbit defined from a file of 3D sample points, in Km. |brsp| See :ref:`orbits` for more information.
     - ``provider``
   * - ``orbit``
     - The orbit component, containing some additional information, like the orbital elements, the period, etc. See :ref:`orbits` for a full description of the format and possible values of this attribute.
     - 
   * - ``orientationModel``
     - The orientation model of this orbit. |brsp| **Values:** |br| ``default`` -- the default orientation. |br| ``extrasolar_system`` -- orientation for extrasolar systems. See :ref:`orbits` for more information.
     - ``model``
   * - ``onlyBody``
     - In object-less orbits (orbits not attached to any object), it may be interesting to not render the orbit itself as a line, but only a point at the head of that orbit in the current time. If this attribute is set to true, the orbit is rendered as a single point at the head. Useful essentially to render many particles using orbital elements. **This attribute is deprecated, use bodyRepresentation instead**.
     - ``onlybody``
   * - ``bodyRepresentation``
     - The body representation type for this orbit/trajectory. This only works with orbits defined via orbital elements. |brsp| **Values:** |br| ``only_orbit`` -- the body is not visually represented at all. |br| ``only_body`` -- only the body is visually represented, no line. |br| ``body_and_orbit`` -- both body and orbit line are represented.
     - 
   * - ``bodyColor``
     - Body color. Color to use to represent the body in orbital elements trajectories, when the ``bodyRepresentation`` attribute enables the representation of the body for this trajectory.
     - ``pointColor``, ``pointcolor``
   * - ``pointSize``
     - The size of the point at the head of the trajectory in object-less orbits (orbits that are not attached to any object). Examples of this are asteroids, where orbits are defined via the orbital elements, and not all orbits are attached to an asteroid object for performance purposes. In these cases, the size of the point at the head of the orbit is set in this property.
     - ``pointsize``
   * - ``closedLoop``
     - Define whether the loop must be closed or not (i.e. join the end point with the start point). Defaults to true, which is the value for periodic orbits.
     - 
   * - ``orbitTrail``
     - Whether to fade the orbit as it gets further away from the head (or object), in the opposite direction of travel. By default, the head is mapped to an opacity of 1, and the tail is mapped to an opacity of 0.
     - ``orbittrail``, ``trail``
   * - ``trailMap``
     - Modify the tail mapping value in case ``orbitTrail`` is set to ``true``. This mapping parameter defines the location in the orbit (in [0,1]) where we map the opacity value of 0. The default value is 0. Set it to 0.5 to have a trail from the object to half the orbit. This enables having shorter trails, while improving the performance due to less lines being rendered.
     - 
   * - ``trailMinOpacity``
     - Minimum opacity level of the whole orbit in trail mode. Only active if ``orbitTrail`` is set to ``true``. This parameter defines the minimum opacity of the orbit (in [0,1]). This enables having trails where the faint end maps to an opacity larger than zero, thus getting to see the whole orbit at all times.
     - 
   * - ``newMethod``
     - Internal parameter. Changes the way in which transformations are applied to the orbit objects. Asteroids have this set to true.
     - ``newmethod``
   * - ``numSamples``
     - Override the number of samples to be used for this orbit. Gaia Sky computes the number of samples internally, but in some cases it may be necessary to override it to give orbits more detail.
     - 
   * - ``orbitScaleFactor``
     - Multiplicative factor to scale the orbit data points when they are being loaded.
     - ``multiplier``
   * - ``fadeDistanceUp``
     - Override the distance at which the orbit becomes totally invisible. In internal units. If not overridden, this is computed internally from the radius of the attached body.
     - 
   * - ``fadeDistanceDown``
     - Override the distance at which the orbit starts to fade out. In internal units. If not overridden, this is computed internally from the radius of the attached body.
     - 
   * - ``refreshRate``
     - For orbits that need to be refreshed (i.e. not implemented as orbital elements, but via samples), this is the orbit refresh rate, in [0,1]. Set to 0 to recompute only every period, and set to 1 to recompute as often as possible. Set to negative to use the default re-computation heuristic. This can help reduce the visible seams that occur in the transitions from the trajectory lines computed in the period to the current period in orbits which are very open.
     - 


.. _comp-ModelScaffolding:

ModelScaffolding
~~~~~~~~~~~~~~~~

Defines attributes related objects with 3D models.

This component is in the following archetypes: :ref:`ModelBody <arch-ModelBody>`, :ref:`Star <arch-Star>`.

.. list-table:: ModelScaffolding attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``referencePlane``
     - The reference plane to use for this model object. |brsp| **Values:** |br| ``ecliptic`` |br| ``galactic`` |br| ``equatorial``
     - ``refPlane``, ``refplane``
   * - ``randomize``
     - A list with the components of this model that need to be randomized via procedural generation. Can contain 'model', 'atmosphere', and/or 'cloud'.
     - 
   * - ``seed``
     - In case the 'randomize' attribute is defined, this attribute defines the RNG seed to use.
     - 
   * - ``sizeScaleFactor``
     - Scale factor to apply to the 3D model of this object. Mainly used internally. Using the model or object attributes directly to specify the size is recommended.
     - ``sizescalefactor``
   * - ``locVaMultiplier``
     - Solid angle multiplier for children location objects (Loc) of this object. If set, this scales the solid angle of the object for children locations.
     - ``locvamultiplier``
   * - ``locThresholdLabel``
     - Threshold label value for children locations. Mainly used internally, should not be touched externally.
     - ``locThOverFactor``, ``locthoverfactor``
   * - ``selfShadow``
     - Whether to render self-shadows for this object.
     - 
   * - ``shadowValues``
     - Deprecated as of Gaia Sky 3.6.1.
     - ``shadowvalues``


.. _comp-Model:

Model
~~~~~

Defines the actual model of objects with 3D models. See :ref:`model` for more information.

This component is in the following archetypes: :ref:`ModelBody <arch-ModelBody>`, :ref:`Star <arch-Star>`, :ref:`StarCluster <arch-StarCluster>`, :ref:`MeshObject <arch-MeshObject>`, :ref:`BackgroundModel <arch-BackgroundModel>`, :ref:`RecursiveGrid <arch-RecursiveGrid>`, :ref:`StarGroup <arch-StarGroup>`, :ref:`Model <arch-Model>`, :ref:`VRDeviceModel <arch-VRDeviceModel>`.

.. list-table:: Model attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``model``
     - Model definition for this object. See the :ref:`model` documentation for more information.
     - 


.. _comp-Volume:

Volume
~~~~~~

Defines a volume to be rendered inside a model, typically a bounding box.

This component is in the following archetypes: :ref:`Volume <arch-Volume>`.

.. list-table:: Volume attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``vertexShader``
     - The vertex shader to use. Prefix with `$data` to use a shader in a dataset. Defaults to the PBR vertex shader.
     - 
   * - ``fragmentShader``
     - The fragment shader to use. Prefix with `$data` to use a shader in a dataset.
     - 


.. _comp-Atmosphere:

Atmosphere
~~~~~~~~~~

Defines the atmosphere of a planet or moon. See the :ref:`atmospheric-scattering` documentation for more information.

This component is in the following archetypes: :ref:`Planet <arch-Planet>`.

.. list-table:: Atmosphere attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``atmosphere``
     - Atmosphere definition for this object. See the :ref:`atmospheric-scattering` documentation for more information.
     - 


.. _comp-Cloud:

Cloud
~~~~~

Defines the cloud layer of a planet or moon. See the :ref:`clouds` documentation for more information.

This component is in the following archetypes: :ref:`Planet <arch-Planet>`.

.. list-table:: Cloud attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``cloud``
     - Cloud layer definition for this object. See the :ref:`clouds` documentation for more information.
     - 


.. _comp-RenderFlags:

RenderFlags
~~~~~~~~~~~

Defines rendering flags.

This component is in the following archetypes: :ref:`GenericSpacecraft <arch-GenericSpacecraft>`.

.. list-table:: RenderFlags attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``renderQuad``
     - Whether to render this entity as a billboard (quad).
     - ``renderquad``


.. _comp-MotorEngine:

MotorEngine
~~~~~~~~~~~

Defines machines for the spacecraft mode.

This component is in the following archetypes: :ref:`Spacecraft <arch-Spacecraft>`.

.. list-table:: MotorEngine attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``machines``
     - Provides machine definitions for the spacecraft mode. Check out the spacecraft object definition in the default data pack for more information.
     - 


.. _comp-RefSysTransform:

RefSysTransform
~~~~~~~~~~~~~~~

Defines an arbitrary reference system transformation via a 4x4 matrix. See :ref:`ref-sys-transform` for more information.

This component is in the following archetypes: :ref:`Orbit <arch-Orbit>`, :ref:`GenericCatalog <arch-GenericCatalog>`, :ref:`MeshObject <arch-MeshObject>`, :ref:`BackgroundModel <arch-BackgroundModel>`, :ref:`RecursiveGrid <arch-RecursiveGrid>`, :ref:`Axes <arch-Axes>`, :ref:`Model <arch-Model>`.

.. list-table:: RefSysTransform attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``transformFunction``
     - Defines a transformation matrix to apply to the position of the object. The name of the transformation to apply. |brsp| **Values:** |br| ``equatorialToEcliptic`` |br| ``eclipticToEquatorial`` |br| ``equatorialToGalactic`` |br| ``galacticToEquatorial`` |br| ``eclipticToGalactic`` |br| ``galacticToEcliptic`` |brsp| See :ref:`ref-sys-transform` for more information.
     - ``transformName``
   * - ``transformMatrix``
     - The 16 values of the 4x4 transformation matrix, in column-major order. See :ref:`ref-sys-transform` for more information.
     - ``transformValues``


.. _comp-AffineTransformations:

AffineTransformations
~~~~~~~~~~~~~~~~~~~~~

Defines arbitrary affine transformations, applied in the order they are defined. See :ref:`affine-transformations` for more information.

This component is in the following archetypes: :ref:`ModelBody <arch-ModelBody>`, :ref:`Orbit <arch-Orbit>`, :ref:`GenericCatalog <arch-GenericCatalog>`, :ref:`MeshObject <arch-MeshObject>`, :ref:`OctreeWrapper <arch-OctreeWrapper>`, :ref:`Model <arch-Model>`.

.. list-table:: AffineTransformations attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``matrix``
     - A generic 4x4 matrix transform that will be applied to the sequence of affine transformations. The matrix values need to be in column-major order. See :ref:`affine-transformations` for more information.
     - ``transformMatrix``
   * - ``translate``
     - A translation vector, in internal units (see aliases for other units). See :ref:`affine-transformations` for more information.
     - ``translatePc``, ``translateKm``
   * - ``rotate``
     - A rotation axis and angle, in degrees. The vector is expected as [X, Y, Z, angle]. See :ref:`affine-transformations` for more information.
     - 
   * - ``scale``
     - A scale transformation. Can be a 3-vector or a single value. See :ref:`affine-transformations` for more information.
     - 
   * - ``transformations``
     - Describe the transformations directly in a map, with 'impl', and whatever attributes. The usage of the attributes 'translate', 'scale' and 'rotate' is strongly recommended over this.
     - 


.. _comp-Fade:

Fade
~~~~

Defines the properties that control the fading in and out of the object.

This component is in the following archetypes: :ref:`Billboard <arch-Billboard>`, :ref:`FadeNode <arch-FadeNode>`, :ref:`RecursiveGrid <arch-RecursiveGrid>`, :ref:`Text2D <arch-Text2D>`, :ref:`OctreeWrapper <arch-OctreeWrapper>`.

.. list-table:: Fade attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``fadeIn``
     - The starting and ending fade-in distances, in parsecs, from the reference system origin (unless 'fadeObjectName' or 'fadePosition' are defined, in which case the distances are relative to the given object or position), where the object starts and ends its fade-in transition. 
     - ``fadein``
   * - ``fadeInMap``
     - The alpha/opacity values to which the fade-in starting and ending distances are mapped. They default to [0,1].
     - 
   * - ``fadeOut``
     - The starting and ending fade-out distances, in parsecs, from the reference system origin (unless 'fadeObjectName' or 'fadePosition' are defined, in which case the distances are relative to the given object or position), where the object starts and ends its fade-out transition. 
     - ``fadeout``
   * - ``fadeOutMap``
     - The alpha/opacity values to which the fade-out starting and ending distances are mapped. They default to [1,0].
     - 
   * - ``fadeObjectName``
     - The name of the object to be used to compute the current distance for the fade in and out operations.
     - ``positionobjectname``
   * - ``fadePosition``
     - The position, in the internal reference system and internal units, to be used to compute the current distance for the fade in and out operations.
     - 


.. _comp-DatasetDescription:

DatasetDescription
~~~~~~~~~~~~~~~~~~

Contains metadata about the dataset represented by this object. All objects with this component get an entry in the datasets list.

This component is in the following archetypes: :ref:`GenericCatalog <arch-GenericCatalog>`, :ref:`MeshObject <arch-MeshObject>`, :ref:`OctreeWrapper <arch-OctreeWrapper>`.

.. list-table:: DatasetDescription attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``catalogInfo``
     - A map containing the metadata for the catalog represented by this object. The map can contain the attributes 'name', 'description', 'type' (INTERNAL|SCRIPT|LOD|SAMP|UI), 'nParticles', 'sizebytes'. See :ref:`catalog-formats` for more information.
     - ``datasetInfo``, ``cataloginfo``
   * - ``addDataset``
     - Whether to add the dataset to the dataset manager or not. Typically used with star and particle sets that already belong to a higher-level dataset.
     - ``addToDatasetManager``


.. _comp-Label:

Label
~~~~~

Defines attributes that control how labels are processed and rendered. See :ref:`labels` for more information.

This component is in the following archetypes: :ref:`CelestialBody <arch-CelestialBody>`, :ref:`StarCluster <arch-StarCluster>`, :ref:`Orbit <arch-Orbit>`, :ref:`FadeNode <arch-FadeNode>`, :ref:`BackgroundModel <arch-BackgroundModel>`, :ref:`RecursiveGrid <arch-RecursiveGrid>`, :ref:`Text2D <arch-Text2D>`, :ref:`Loc <arch-Loc>`, :ref:`StarGroup <arch-StarGroup>`, :ref:`Constellation <arch-Constellation>`, :ref:`CosmicRuler <arch-CosmicRuler>`, :ref:`ShapeObject <arch-ShapeObject>`, :ref:`KeyframesPathObject <arch-KeyframesPathObject>`.

.. list-table:: Label attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``label``
     - A boolean to disable or enable label rendering for this object.
     - 
   * - ``label2d``
     - Unused, here for backwards compatibility.
     - 
   * - ``labelPosition``
     - Override the position at which to render this label, in the internal reference system and internal units (see aliases for more unit options). If this is not given, the position of the object is used.
     - ``labelposition``, ``labelPositionPc``, ``labelPositionKm``
   * - ``renderLabel``
     - Defaults to true, this flag enables or disables the actual rendering of the label in the attached object.
     - 
   * - ``forceLabel``
     - Force-display the label of this object, regardless of its solid angle. If 'true', the label for this object is always displayed.
     - 
   * - ``labelFactor``
     - Factor to apply to the size of the label for this object.
     - 
   * - ``labelBias``
     - Bias to compute the label visibility. >1 to increase visibility.
     - 
   * - ``labelMax``
     - Internal rendering factor, should not be set externally.
     - 
   * - ``textScale``
     - Internal rendering factor, should not be set externally.
     - 


.. _comp-RenderType:

RenderType
~~~~~~~~~~

Defines attributes that control rendering operations for this object.

This component is in the following archetypes: :ref:`ModelBody <arch-ModelBody>`, :ref:`Particle <arch-Particle>`, :ref:`BackgroundModel <arch-BackgroundModel>`, :ref:`RecursiveGrid <arch-RecursiveGrid>`, :ref:`Model <arch-Model>`.

.. list-table:: RenderType attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``renderGroup``
     - This is an internal property used to fine-tune exactly the environment and shader to use to render the object. See `RenderGroup.java <https://codeberg.org/gaiasky/gaiasky/src/branch/master/core/src/gaiasky/render/RenderGroup.java>`__ for more information.
     - ``rendergroup``


.. _comp-BillboardSet:

BillboardSet
~~~~~~~~~~~~

Defines attributes related to billboard set objects.

This component is in the following archetypes: :ref:`BillboardGroup <arch-BillboardGroup>`.

.. list-table:: BillboardSet attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``data``
     - A list of ``BillboardDataset`` objects. Mainly used for the Milky Way model. Each object contains 'impl', 'file', 'type', 'size', 'intensity', 'layers', and 'maxsizes'. See the Milky Way object in the ``universe.json`` file in the default data pack for an example.
     - 


.. _comp-Title:

Title
~~~~~

Defines attributes related to two-dimensional titles.

This component is in the following archetypes: :ref:`Text2D <arch-Text2D>`.

.. list-table:: Title attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``scale``
     - Scale factor to scale up or down the title.
     - 
   * - ``lines``
     - Whether to render frame lines above and below the title or not.
     - 
   * - ``align``
     - The horizontal alignment of the title. Center (1), left (8) or right (16).
     - 


.. _comp-Axis:

Axis
~~~~

Defines attributes related to reference system axes.

This component is in the following archetypes: :ref:`Axes <arch-Axes>`.

.. list-table:: Axis attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``axesColors``
     - A 3x3 matrix with the color for each of the axes in the reference system.
     - 


.. _comp-LocationMark:

LocationMark
~~~~~~~~~~~~

Defines attributes related to location objects. Location objects usually mark special points on the surface of objects, like cities, craters, etc. They have a label (text) and an optoinal marker. The label color is defined in the attribute 'labelColor', and the marker color is defined in the attribute 'color'. See :ref:`locations` for more information.

This component is in the following archetypes: :ref:`Loc <arch-Loc>`.

.. list-table:: LocationMark attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``location``
     - A 2-dimensional position [longitude, latitude] on the surface of the parent, in degrees.
     - 
   * - ``tooltipText``
     - Descriptive text to display as a tooltip when the mouse hovers over the location mark. Only locations with markers display tooltips.
     - 
   * - ``link``
     - A link (URL) to an external resource, preferrably Wikipedia, with more information about this location.
     - 
   * - ``locationType``
     - Additional categorization of locations. This is used only in the UI so that all locations in the same category can be turned on and off at the same time with a single click.
     - 
   * - ``ignoreSolidAngleLimit``
     - Ignore the solid angle upper limit when determining the visibility of this location. Setting this to true causes the location to not disappear, regardless of the camera distance.
     - 
   * - ``locationMarkerTexture``
     - Location marker texture (image). Set to 'none' to disable maker. Possible values are 'none', 'default', 'flag', 'city', or a path to a PNG image. If the path directs to a data package, the format is '$data/[package-name]/path/to/file.png'.
     - ``markerTexture``
   * - ``distFactor``
     - Factor to apply to the radius of the parent object to get distance from the center of the object, in case of non-spherical parent objects. This modulates the distance of the location from the center of the object (radius), as the locations are given in [longitude, latitude, radius]. If this is not specified, the radius is that of the parent object. If you want the location to show at double the radius distance, use 2.0.
     - 


.. _comp-Constel:

Constel
~~~~~~~

Defines attributes related to constellation objects.

This component is in the following archetypes: :ref:`Constellation <arch-Constellation>`.

.. list-table:: Constel attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``ids``
     - Contains a list of segments (a list of lists of points) with the HIP identifiers for each of the stars of this constellation.
     - 


.. _comp-Boundaries:

Boundaries
~~~~~~~~~~

Defines attributes related to constellation boundary objects.

This component is in the following archetypes: :ref:`ConstellationBoundaries <arch-ConstellationBoundaries>`.

.. list-table:: Boundaries attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``boundaries``
     - Contains a list of lists of sky coordinates :math:`(\alpha, \delta)`, in degrees, defining the lines of the constellation boundaries.
     - ``boundariesEquatorial``


.. _comp-ParticleSet:

ParticleSet
~~~~~~~~~~~

Defines attributes related to particle set objects, which contain a point cloud.

This component is in the following archetypes: :ref:`ParticleGroup <arch-ParticleGroup>`.

.. list-table:: ParticleSet attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``provider``
     - The class to be used to load the data. This class must implement ``IParticleGroupDataProvider``. This should have the fully-qualified class name. For instance, ``gaiasky.data.group.STILDataProvider``.
     - 
   * - ``providerParams``
     - Parameters to be passed into the provider class.
     - ``providerparams``
   * - ``meanPosition``
     - The mean position of this particle set, in the internal reference system and internal units (see aliases for more units). If not given, this is computed automatically from the particle positions.
     - ``meanPositionKm``, ``meanPositionPc``, ``pos``, ``posKm``, ``posPc``, ``position``
   * - ``dataFile``
     - The path to the data file with the particles to be loaded by the provider.
     - ``datafile``
   * - ``factor``
     - A multiplicative factor to apply to the positions of all particles during loading.
     - 
   * - ``numLabels``
     - Number of labels to render for this particle group. Defaults to the configuration setting.
     - 
   * - ``profileDecay``
     - The profile decay of the particles in the shader. Controls how sudden is the color and intensity falloff from the center.
     - ``profiledecay``
   * - ``colorNoise``
     - Noise factor for the color, in [0,1]. This randomly generates colors from the main color. The larger the color noise, the more different the generated colors from the main color.
     - ``colornoise``
   * - ``particleSizeLimits``
     - Minimum and maximum solid angle limits of the particles in radians. They are used as :math:`(dist * tan(\alpha_{min}), dist * tan(\alpha_{max}))`. The minimum and maximum values must be in [0,1.57].
     - ``particlesizelimits``, ``particleSizeLimitsDeg``
   * - ``colorMin``
     - The color of the particles at the closest distance, as RGBA. If this is set, the color of the particles gets interpolated from ``colorMin`` to ``colorMax`` depending on the distance of the particles to the origin.
     - 
   * - ``colorMax``
     - The color of the particles at the maximum distance, as RGBA. If this is set, the color of the particles gets interpolated from ``colorMin`` to ``colorMax`` depending on the distance of the particles to the origin.
     - 
   * - ``colorFromTexture``
     - If true, color of this particle depends on the texture assigned to it. This is useful when using 'textureAttribute', for instance, where the texture is assigned depending on the value of an attribute for this object. This feature requires a non-zero 'colorNoise', as it is used to generate the colors.
     - 
   * - ``fixedAngularSize``
     - Set a fixed angular size for all particles in this set, as a solid angle in radians (see aliases for other units).
     - ``fixedAngularSizeDeg``, ``fixedAngularSizeRad``
   * - ``renderSetLabel``
     - Enable or disable the global label of this particle set. If true, the name of this particle set is rendered at the given label position.
     - 
   * - ``renderParticles``
     - Disable particle rendering by setting this to false. Labels, in case of star sets, will still be rendered.
     - 
   * - ``texture``
     - Texture file to render the particles of this group. This can also point to a directory, in which case all the image files within are used (they must have the same dimensions). If this is provided, ``profileDecay`` is ignored.
     - 
   * - ``textures``
     - List of texture files to render the particles of this group. If more than one texture is provided, each particle is assigned a texture randomly. This can also point to one or more directories, in which case all the image files within are used. All images must have the same dimensions. If this is provided, ``profileDecay`` is ignored.
     - 
   * - ``textureAttribute``
     - If present, this attribute is used to assign textures to particles. It should be an integer attribute in [1,n], where n is the number of textures. If the value of the attribute is out of this range, it is clamped. The attribute value is used as an index to query the texture array, where the textures are sorted using the natural order of their file names. If the attribute is of any other type, Gaia Sky will do its best to use it to assign textures as well, but no guarantees.
     - 
   * - ``modelFile``
     - Path to the model file to use (``obj``, ``obj.gz`` ``g3db``, ``g3dj``, ``gltf``, ``glb``). If present, the ``modelType`` and ``modelParams`` attributes are ignored. The model should have only positions (vector-3), normals (vector-3), and texture coordinates (vector-2) as vertex attributes. Only the first mesh of the model is used. Textures, lighting and material are ignored. This is only used in extended particle groups.
     - 
   * - ``modelType``
     - The model type to use for this particle set. |brsp| **Values:** |br| ``quad`` -- render billboards. |br| ``sphere`` render UV spheres. |br| ``icosphere`` render icosahedron-based spheres. |brsp| Defaults to 'quad'. This is only used in extended particle groups. To enable extended particle groups, you need to set 'type' to 'PARTICLES_EXT' in the 'providerParams' map.
     - 
   * - ``modelParams``
     - Model parameters in a map. Usually, the 'diameter', 'width', 'height', 'recursion' or 'quality' go here. For more info, see the  ``gaiasky.util.ModeCache`` class. This is only used in extended particle groups.
     - 
   * - ``modelPrimitive``
     - The GL primitive to use. |brsp| **Values:** |br| ``GL_TRIANGLES`` |br| ``GL_TRIANGLE_STRIP`` |br| ``GL_TRIANGLE_FAN`` |br| ``GL_LINES`` |br| ``GL_LINE_STRIP`` |br| ``GL_LINE_LOOP`` |brsp| Defaults to ``GL_TRIANGLES``. The ``GL_LINE`` primitives enable wireframe rendering, which is currently only supported by sphere and ico-sphere model types. This is only used in extended particle groups.
     - 
   * - ``proximityDescriptorsLocation``
     - Location of directory that contains descriptor JSON files that bear the names of objects in the dataset. These get loaded whenever the camera gets close to a particle.
     - ``proximityDescriptors``, ``descriptorsLocation``
   * - ``proximityThreshold``
     - Solid angle above which the proximity descriptor loading is triggered.
     - ``proximityThresholdDeg``, ``proximityThresholdRad``


.. _comp-StarSet:

StarSet
~~~~~~~

Defines attributes related to star set objects, which contain a star catalog or group.

This component is in the following archetypes: :ref:`StarGroup <arch-StarGroup>`.

.. list-table:: StarSet attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``provider``
     - The class to be used to load the data. This class must implement ``IParticleGroupDataProvider``. This should have the fully-qualified class name. For instance, ``gaiasky.data.group.STILDataProvider``.
     - 
   * - ``providerParams``
     - Parameters to be passed into the provider class.
     - ``providerparams``
   * - ``meanPosition``
     - The mean position of this particle set, in the internal reference system and internal units (see aliases for more units). If not given, this is computed automatically from the particle positions.
     - ``meanPositionKm``, ``meanPositionPc``, ``pos``, ``posKm``, ``posPc``, ``position``
   * - ``dataFile``
     - The path to the data file with the particles to be loaded by the provider.
     - ``datafile``
   * - ``factor``
     - A multiplicative factor to apply to the positions of all particles during loading.
     - 
   * - ``profileDecay``
     - The profile decay of the particles in the shader. Controls how sudden is the color and intensity falloff from the center.
     - ``profiledecay``
   * - ``colorNoise``
     - Noise factor for the color, in [0,1]. This randomly generates colors from the main color. The larger the color noise, the more different the generated colors from the main color.
     - ``colornoise``
   * - ``particleSizeLimits``
     - Minimum and maximum solid angle limits of the particles in radians. They are used as :math:`(dist * tan(\alpha_{min}), dist * tan(\alpha_{max}))`. The minimum and maximum values must be in [0,1.57].
     - ``particlesizelimits``, ``particleSizeLimitsDeg``
   * - ``colorMin``
     - The color of the particles at the closest distance, as RGBA. If this is set, the color of the particles gets interpolated from ``colorMin`` to ``colorMax`` depending on the distance of the particles to the origin.
     - 
   * - ``colorMax``
     - The color of the particles at the maximum distance, as RGBA. If this is set, the color of the particles gets interpolated from ``colorMin`` to ``colorMax`` depending on the distance of the particles to the origin.
     - 
   * - ``fixedAngularSize``
     - Set a fixed angular size for all particles in this set, as a solid angle in radians (see aliases for other units).
     - ``fixedAngularSizeDeg``, ``fixedAngularSizeRad``
   * - ``renderParticles``
     - Disable particle rendering by setting this to false. Labels, in case of star sets, will still be rendered.
     - 
   * - ``epochJd``
     - The epoch for the positions of this star group as a Julian date.
     - ``epoch``
   * - ``variabilityEpochJd``
     - The light curve epoch for the variable stars in this star group as a Julian date.
     - ``variabilityEpoch``
   * - ``numLabels``
     - Number of labels to render for this star group. Defaults to the configuration setting.
     - 


.. _comp-ParticleExtra:

ParticleExtra
~~~~~~~~~~~~~

Defines attributes related to single particles and single star objects.

This component is in the following archetypes: :ref:`Particle <arch-Particle>`.

.. list-table:: ParticleExtra attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``primitiveRenderScale``
     - Artificial scale factor for the size of this particle during rendering.
     - 
   * - ``tEff``
     - Effective temperature of the star or body, in Kelvin.
     - ``teff``


.. _comp-Mesh:

Mesh
~~~~

Defines attributes related to meshes and iso-density surfaces. See :ref:`mesh-objects` for more information.

This component is in the following archetypes: :ref:`MeshObject <arch-MeshObject>`.

.. list-table:: Mesh attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``shading``
     - Shading mode for the mesh. |brsp| **Values:** |br| ``additive`` -- additive blending. |br| ``dust`` -- opaque mesh with dither transparency at the edges. |br| ``regular`` -- regular general-purpose PBR shader.
     - 
   * - ``additiveBlending``
     - Sets the shading mode to 'additive'.
     - ``additiveblending``


.. _comp-Focus:

Focus
~~~~~

Defines attributes related to objects that can be focussed.

This component is in the following archetypes: :ref:`CelestialBody <arch-CelestialBody>`, :ref:`StarCluster <arch-StarCluster>`, :ref:`ParticleGroup <arch-ParticleGroup>`, :ref:`StarGroup <arch-StarGroup>`, :ref:`Model <arch-Model>`.

.. list-table:: Focus attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``focusable``
     - Defines whether the object is focusable or not. Non-focusable objects do not appear in the search results and can't be selected with the mouse. By default, this is true.
     - 


.. _comp-Raymarching:

Raymarching
~~~~~~~~~~~

Defines attributes related to ray-marched objects.

This component is in the following archetypes: :ref:`Invisible <arch-Invisible>`.

.. list-table:: Raymarching attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``shader``
     - Path to the fragment shader GLSL file to use to render this object. The fragment shader is processed for each pixel in the image, and must produce a ray-marched representation of the object. The file must have one of the following extensions: ``.glsl``, ``.frag``, ``.fragment``, ``.glslf``, ``.fsh``. The fragment shader file is typically distributed with the dataset, and has the form ``$data/[dataset-name]/path/to/file.glsl``.
     - ``raymarchingShader``
   * - ``additionalTexture``
     - Texture file to pass to the raymarching shader as additional texture. This is usually a noise texture, but can be anything, really.
     - ``raymarchingTexture``


.. _comp-Highlight:

Highlight
~~~~~~~~~

Defines attributes that apply to the visual representation of particle and star sets.

This component is in the following archetypes: :ref:`GenericCatalog <arch-GenericCatalog>`, :ref:`OctreeWrapper <arch-OctreeWrapper>`.

.. list-table:: Highlight attributes
   :widths: 25 50 25
   :header-rows: 1
   :class: tight-table

   * - **Attribute**
     - **Description**
     - **Aliases**
   * - ``pointScaling``
     - Scale factor taht applies to the the visual representation for each object of this dataset.
     - 

.. include:: ../global.rst
