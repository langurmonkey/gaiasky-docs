.. _archetypes:

Archetypes
----------

Below is a table with all the archetypes in Gaia Sky. For each archetype, we list its parent (if any) and its :ref:`components`.

A general description of archetypes and components is provided in :ref:`data-morphology`.


All archetypes are: :ref:`SceneGraphNode <arch-SceneGraphNode>`, :ref:`Universe <arch-Universe>`, :ref:`CelestialBody <arch-CelestialBody>`, :ref:`ModelBody <arch-ModelBody>`, :ref:`Planet <arch-Planet>`, :ref:`Volume <arch-Volume>`, :ref:`Particle <arch-Particle>`, :ref:`Star <arch-Star>`, :ref:`Satellite <arch-Satellite>`, :ref:`HeliotropicSatellite <arch-HeliotropicSatellite>`, :ref:`GenericSpacecraft <arch-GenericSpacecraft>`, :ref:`Spacecraft <arch-Spacecraft>`, :ref:`StarCluster <arch-StarCluster>`, :ref:`Billboard <arch-Billboard>`, :ref:`BillboardGalaxy <arch-BillboardGalaxy>`, :ref:`VertsObject <arch-VertsObject>`, :ref:`Polyline <arch-Polyline>`, :ref:`Orbit <arch-Orbit>`, :ref:`HeliotropicOrbit <arch-HeliotropicOrbit>`, :ref:`FadeNode <arch-FadeNode>`, :ref:`GenericCatalog <arch-GenericCatalog>`, :ref:`MeshObject <arch-MeshObject>`, :ref:`BackgroundModel <arch-BackgroundModel>`, :ref:`SphericalGrid <arch-SphericalGrid>`, :ref:`RecursiveGrid <arch-RecursiveGrid>`, :ref:`BillboardGroup <arch-BillboardGroup>`, :ref:`Text2D <arch-Text2D>`, :ref:`Axes <arch-Axes>`, :ref:`Loc <arch-Loc>`, :ref:`Area <arch-Area>`, :ref:`ParticleGroup <arch-ParticleGroup>`, :ref:`StarGroup <arch-StarGroup>`, :ref:`Constellation <arch-Constellation>`, :ref:`ConstellationBoundaries <arch-ConstellationBoundaries>`, :ref:`CosmicRuler <arch-CosmicRuler>`, :ref:`OrbitalElementsGroup <arch-OrbitalElementsGroup>`, :ref:`Invisible <arch-Invisible>`, :ref:`OctreeWrapper <arch-OctreeWrapper>`, :ref:`Model <arch-Model>`, :ref:`ShapeObject <arch-ShapeObject>`, :ref:`KeyframesPathObject <arch-KeyframesPathObject>`, :ref:`VRDeviceModel <arch-VRDeviceModel>`.

.. list-table:: Archetypes table
   :header-rows: 1

   * - **Archetype**
     - **Parent**
     - **Components**

       .. _arch-SceneGraphNode:
   * - **SceneGraphNode**
     - 
     - | :ref:`Base <comp-Base>`
       | :ref:`Body <comp-Body>`
       | :ref:`GraphNode <comp-GraphNode>`
       | Octant
       | Render

       .. _arch-Universe:
   * - **Universe**
     - 
     - | :ref:`Base <comp-Base>`
       | :ref:`Body <comp-Body>`
       | :ref:`GraphNode <comp-GraphNode>`
       | GraphRoot

       .. _arch-CelestialBody:
   * - **CelestialBody**
     - :ref:`SceneGraphNode <arch-SceneGraphNode>`
     - | :ref:`Celestial <comp-Celestial>`
       | :ref:`Magnitude <comp-Magnitude>`
       | :ref:`Coordinates <comp-Coordinates>`
       | :ref:`Orientation <comp-Orientation>`
       | :ref:`Label <comp-Label>`
       | :ref:`SolidAngle <comp-SolidAngle>`
       | :ref:`Focus <comp-Focus>`
       | Billboard

       .. _arch-ModelBody:
   * - **ModelBody**
     - :ref:`CelestialBody <arch-CelestialBody>`
     - | :ref:`Model <comp-Model>`
       | :ref:`RenderType <comp-RenderType>`
       | :ref:`ModelScaffolding <comp-ModelScaffolding>`
       | :ref:`AffineTransformations <comp-AffineTransformations>`

       .. _arch-Planet:
   * - **Planet**
     - :ref:`ModelBody <arch-ModelBody>`
     - | :ref:`Atmosphere <comp-Atmosphere>`
       | :ref:`Cloud <comp-Cloud>`

       .. _arch-Volume:
   * - **Volume**
     - :ref:`ModelBody <arch-ModelBody>`
     - | :ref:`Volume <comp-Volume>`

       .. _arch-Particle:
   * - **Particle**
     - :ref:`CelestialBody <arch-CelestialBody>`
     - | :ref:`ProperMotion <comp-ProperMotion>`
       | :ref:`RenderType <comp-RenderType>`
       | :ref:`ParticleExtra <comp-ParticleExtra>`

       .. _arch-Star:
   * - **Star**
     - :ref:`Particle <arch-Particle>`
     - | Hip
       | Distance
       | :ref:`Model <comp-Model>`
       | :ref:`ModelScaffolding <comp-ModelScaffolding>`

       .. _arch-Satellite:
   * - **Satellite**
     - :ref:`ModelBody <arch-ModelBody>`
     - | ParentOrientation

       .. _arch-HeliotropicSatellite:
   * - **HeliotropicSatellite**
     - :ref:`Satellite <arch-Satellite>`
     - | TagHeliotropic

       .. _arch-GenericSpacecraft:
   * - **GenericSpacecraft**
     - :ref:`Satellite <arch-Satellite>`
     - | :ref:`RenderFlags <comp-RenderFlags>`

       .. _arch-Spacecraft:
   * - **Spacecraft**
     - :ref:`GenericSpacecraft <arch-GenericSpacecraft>`
     - | :ref:`MotorEngine <comp-MotorEngine>`

       .. _arch-StarCluster:
   * - **StarCluster**
     - :ref:`SceneGraphNode <arch-SceneGraphNode>`
     - | :ref:`Model <comp-Model>`
       | Cluster
       | :ref:`SolidAngle <comp-SolidAngle>`
       | :ref:`ProperMotion <comp-ProperMotion>`
       | :ref:`Label <comp-Label>`
       | :ref:`Focus <comp-Focus>`
       | Billboard

       .. _arch-Billboard:
   * - **Billboard**
     - :ref:`ModelBody <arch-ModelBody>`
     - | TagBillboardSimple
       | :ref:`Fade <comp-Fade>`

       .. _arch-BillboardGalaxy:
   * - **BillboardGalaxy**
     - :ref:`Billboard <arch-Billboard>`
     - | TagBillboardGalaxy

       .. _arch-VertsObject:
   * - **VertsObject**
     - :ref:`SceneGraphNode <arch-SceneGraphNode>`
     - | Verts

       .. _arch-Polyline:
   * - **Polyline**
     - :ref:`VertsObject <arch-VertsObject>`
     - | Arrow
       | Line

       .. _arch-Orbit:
   * - **Orbit**
     - :ref:`Polyline <arch-Polyline>`
     - | :ref:`Trajectory <comp-Trajectory>`
       | :ref:`RefSysTransform <comp-RefSysTransform>`
       | :ref:`AffineTransformations <comp-AffineTransformations>`
       | :ref:`Label <comp-Label>`

       .. _arch-HeliotropicOrbit:
   * - **HeliotropicOrbit**
     - :ref:`Orbit <arch-Orbit>`
     - | TagHeliotropic

       .. _arch-FadeNode:
   * - **FadeNode**
     - :ref:`SceneGraphNode <arch-SceneGraphNode>`
     - | :ref:`Fade <comp-Fade>`
       | :ref:`Label <comp-Label>`

       .. _arch-GenericCatalog:
   * - **GenericCatalog**
     - :ref:`FadeNode <arch-FadeNode>`
     - | :ref:`DatasetDescription <comp-DatasetDescription>`
       | Highlight
       | :ref:`RefSysTransform <comp-RefSysTransform>`
       | :ref:`AffineTransformations <comp-AffineTransformations>`

       .. _arch-MeshObject:
   * - **MeshObject**
     - :ref:`FadeNode <arch-FadeNode>`
     - | :ref:`Mesh <comp-Mesh>`
       | :ref:`Model <comp-Model>`
       | :ref:`DatasetDescription <comp-DatasetDescription>`
       | :ref:`RefSysTransform <comp-RefSysTransform>`
       | :ref:`AffineTransformations <comp-AffineTransformations>`

       .. _arch-BackgroundModel:
   * - **BackgroundModel**
     - :ref:`FadeNode <arch-FadeNode>`
     - | TagBackgroundModel
       | :ref:`RefSysTransform <comp-RefSysTransform>`
       | :ref:`Model <comp-Model>`
       | :ref:`Label <comp-Label>`
       | :ref:`Coordinates <comp-Coordinates>`
       | :ref:`RenderType <comp-RenderType>`

       .. _arch-SphericalGrid:
   * - **SphericalGrid**
     - :ref:`BackgroundModel <arch-BackgroundModel>`
     - | GridUV

       .. _arch-RecursiveGrid:
   * - **RecursiveGrid**
     - :ref:`SceneGraphNode <arch-SceneGraphNode>`
     - | GridRecursive
       | :ref:`Fade <comp-Fade>`
       | :ref:`RefSysTransform <comp-RefSysTransform>`
       | :ref:`Model <comp-Model>`
       | :ref:`Label <comp-Label>`
       | Line
       | :ref:`RenderType <comp-RenderType>`

       .. _arch-BillboardGroup:
   * - **BillboardGroup**
     - :ref:`GenericCatalog <arch-GenericCatalog>`
     - | :ref:`BillboardSet <comp-BillboardSet>`
       | :ref:`Coordinates <comp-Coordinates>`

       .. _arch-Text2D:
   * - **Text2D**
     - :ref:`SceneGraphNode <arch-SceneGraphNode>`
     - | :ref:`Fade <comp-Fade>`
       | :ref:`Title <comp-Title>`
       | :ref:`Label <comp-Label>`

       .. _arch-Axes:
   * - **Axes**
     - :ref:`SceneGraphNode <arch-SceneGraphNode>`
     - | :ref:`Axis <comp-Axis>`
       | :ref:`RefSysTransform <comp-RefSysTransform>`
       | Line

       .. _arch-Loc:
   * - **Loc**
     - :ref:`SceneGraphNode <arch-SceneGraphNode>`
     - | :ref:`LocationMark <comp-LocationMark>`
       | :ref:`Label <comp-Label>`

       .. _arch-Area:
   * - **Area**
     - :ref:`SceneGraphNode <arch-SceneGraphNode>`
     - | Perimeter
       | Line
       | TagNoProcessGraph

       .. _arch-ParticleGroup:
   * - **ParticleGroup**
     - :ref:`GenericCatalog <arch-GenericCatalog>`
     - | :ref:`ParticleSet <comp-ParticleSet>`
       | TagNoProcessChildren
       | :ref:`Focus <comp-Focus>`

       .. _arch-StarGroup:
   * - **StarGroup**
     - :ref:`GenericCatalog <arch-GenericCatalog>`
     - | :ref:`StarSet <comp-StarSet>`
       | :ref:`Model <comp-Model>`
       | :ref:`Label <comp-Label>`
       | Line
       | :ref:`Focus <comp-Focus>`
       | Billboard

       .. _arch-Constellation:
   * - **Constellation**
     - :ref:`SceneGraphNode <arch-SceneGraphNode>`
     - | :ref:`Constel <comp-Constel>`
       | Line
       | :ref:`Label <comp-Label>`
       | TagNoProcessGraph

       .. _arch-ConstellationBoundaries:
   * - **ConstellationBoundaries**
     - :ref:`SceneGraphNode <arch-SceneGraphNode>`
     - | :ref:`Boundaries <comp-Boundaries>`
       | Line

       .. _arch-CosmicRuler:
   * - **CosmicRuler**
     - :ref:`SceneGraphNode <arch-SceneGraphNode>`
     - | Ruler
       | Line
       | :ref:`Label <comp-Label>`

       .. _arch-OrbitalElementsGroup:
   * - **OrbitalElementsGroup**
     - :ref:`GenericCatalog <arch-GenericCatalog>`
     - | OrbitElementsSet
       | TagNoProcessChildren

       .. _arch-Invisible:
   * - **Invisible**
     - :ref:`CelestialBody <arch-CelestialBody>`
     - | :ref:`Raymarching <comp-Raymarching>`
       | TagInvisible

       .. _arch-OctreeWrapper:
   * - **OctreeWrapper**
     - :ref:`SceneGraphNode <arch-SceneGraphNode>`
     - | :ref:`Fade <comp-Fade>`
       | :ref:`DatasetDescription <comp-DatasetDescription>`
       | Highlight
       | Octree
       | Octant
       | TagNoProcessChildren
       | :ref:`AffineTransformations <comp-AffineTransformations>`

       .. _arch-Model:
   * - **Model**
     - :ref:`SceneGraphNode <arch-SceneGraphNode>`
     - | :ref:`Model <comp-Model>`
       | :ref:`Focus <comp-Focus>`
       | :ref:`RenderType <comp-RenderType>`
       | :ref:`Coordinates <comp-Coordinates>`
       | :ref:`SolidAngle <comp-SolidAngle>`
       | :ref:`RefSysTransform <comp-RefSysTransform>`
       | :ref:`AffineTransformations <comp-AffineTransformations>`

       .. _arch-ShapeObject:
   * - **ShapeObject**
     - :ref:`Model <arch-Model>`
     - | :ref:`Shape <comp-Shape>`
       | :ref:`Label <comp-Label>`
       | Line

       .. _arch-KeyframesPathObject:
   * - **KeyframesPathObject**
     - :ref:`VertsObject <arch-VertsObject>`
     - | Keyframes
       | :ref:`Label <comp-Label>`

       .. _arch-VRDeviceModel:
   * - **VRDeviceModel**
     - :ref:`SceneGraphNode <arch-SceneGraphNode>`
     - | VRDevice
       | :ref:`Model <comp-Model>`
       | Line
       | TagNoClosest

.. include:: ../global.rst
