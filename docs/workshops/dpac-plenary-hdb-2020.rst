.. _dpac-plenary-hdb-2020:

Video production tutorial (DPAC 2020)
=====================================

This page has been retired. However, you can still browse the tutorial notes in the link below:

*  `Video production tutorial notes (DPAC 2020) <https://gaia.ari.uni-heidelberg.de/gaiasky/docs/3.5.8/workshops/dpac-plenary-hdb-2020.html>`__
