Gaia Sky documentation
----------------------

These are the official documentation pages of Gaia Sky. Find below the contents table to navigate around.

-  Visit our home page `gaiasky.space <https://gaiasky.space>`__
-  Download `Gaia Sky <https://gaiasky.space/downloads>`__
-  Submit a `bug or a feature request <https://codeberg.org/gaiasky/gaiasky/issues>`__

You can find a PDF version of this documentation `here <https://gaia.ari.uni-heidelberg.de/gaiasky/docs-pdf/>`__.

Gaia Sky is (partially) described in the paper `Gaia Sky: Navigating the Gaia Catalog <http://dx.doi.org/10.1109/TVCG.2018.2864508>`__.


Contents
========

.. toctree::
   :maxdepth: 3

   Installation
   System-directories
   Quick-start-guide
   User-manual
   Advanced-topics
   Gaia-sky-vr
   Tutorials-workshops
   FAQ
   API reference <https://gaia.ari.uni-heidelberg.de/gaiasky/docs/javadoc/latest/>
   Changelog
   Contact-information
