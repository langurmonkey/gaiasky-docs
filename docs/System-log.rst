.. _log:

.. raw:: html

   <div class="new">Since 3.2.0</div>

System logs
***********

Gaia Sky provides a couple of ways of accessing system logs.

Session log
===========

Gaia Sky always saves the log of the last session to ``$GS_DATA/log/gaiasky_log_lastsession.log`` (check where ``$GS_DATA`` is :ref:`here <folders>`). If you need to check the full log of your last session, you can always find it there.

Crash reports
=============

If Gaia Sky crashes, a crash report, together with a full session log to ``$GS_DATA/crashreports`` (check where ``$GS_DATA`` is :ref:`here <folders>`). Files with the form ``gaiasky_crash_[date].txt`` are crash reports, while files with the form ``gaiasky_log_[date].txt`` are full session logs. You can attach these whenever a crash happens and you want to submit a bug report to our `buck tracker <https://codeberg.org/gaiasky/gaiasky/issues>`__.
