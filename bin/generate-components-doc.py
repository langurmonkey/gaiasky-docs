#! /usr/bin/env python3

import json, os

gs = os.environ.get('GS')

if not gs:
    print("Variable $GS not set. Please point it to your gaiasky directory to proceed.")
    exit(1)

def clean_attribute(val):
    semicolon = val.find(":")
    return val if semicolon < 0 else val[0:semicolon]


attributemap_file = gs + "/assets/archetypes/attributemap.json"
if not os.path.exists(attributemap_file):
    print("Attribute map file does not exist: %s" % attributemap_file)
    exit(1)

archetypes_file = gs + "/assets/archetypes/archetypes.json"
if not os.path.exists(archetypes_file):
    print("Archetypes file does not exist: %s" % archetypes_file)
    exit(1)

fcomponents = open(attributemap_file)
data_components = json.load(fcomponents)
farchetypes = open(archetypes_file)
data_archetypes = json.load(farchetypes)

print(".. _components:")
print()
print("Components")
print("----------")
print()
print("This section lists all components, together with a description and all the attributes. For each attribute, we provide a description and list its units and possible aliases. We also note the :ref:`archetypes` that have the component.")
print()
print("A general description of archetypes and components is provided in :ref:`data-morphology`.")

components = data_components['components']
archetypes = data_archetypes['archetypes']

comps_str = ""
n = len(components)
for i, key in enumerate(components):
    if i > 0:
        comps_str += ", "
    comps_str += ":ref:`" + key + " <comp-" + key + ">`"

print()
print("All components are: %s." % comps_str)
print()


for key in components:
    name = key
    component = components[key]
    print()
    print(".. _comp-%s:" % name)
    print()
    print(name)
    print("~" * len(name))
    print()
    print(component['description'])
    print()
    print("This component is in the following archetypes:", end=" ")
    k = 0
    for arch_key in archetypes:
        archetype = archetypes[arch_key]
        arch_components = archetype["components"]
        num_arch_components = len(arch_components)
        for arch_component in arch_components:
            if name in arch_component:
                if k == 0:
                    # First.
                    print(":ref:`%s <arch-%s>`" % (arch_key, arch_key), end="")
                else:
                    # Not first.
                    print(", :ref:`%s <arch-%s>`" % (arch_key, arch_key), end="")
                k = k + 1
                break
    print(".")
    print()
    print(".. list-table:: %s attributes" % name)
    print("   :widths: 25 50 25")
    print("   :header-rows: 1")
    print("   :class: tight-table")
    print()
    print("   * - **Attribute**")
    print("     - **Description**")
    print("     - **Aliases**")
    for attribute_key in component:
        attribute = component[attribute_key]
        if attribute_key != "description" and attribute != None:
            attribute_clean = clean_attribute(attribute_key)
            print("   * - ``%s``" % attribute_clean)
            print("     - %s" % attribute['description'])
            if 'aliases' not in attribute:
                print("     - ")
            else:
                aliases = ""
                for alias in attribute['aliases']:
                    if len(aliases) == 0:
                        aliases = "``" + clean_attribute(alias) + "``"
                    else:
                        aliases += ", ``" + clean_attribute(alias) + "``"

                print("     - %s" % aliases)

    print()

print(".. include:: ../global.rst")

fcomponents.close()
farchetypes.close()
