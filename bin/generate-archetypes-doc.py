#! /usr/bin/env python3

import json, os

gs = os.environ.get('GS')

if not gs:
    print("Variable $GS not set. Please point it to your gaiasky directory to proceed.")
    exit(1)

def clean_attribute(val):
    semicolon = val.find(":")
    return val if semicolon < 0 else val[0:semicolon]


archetypes_file = gs + "/assets/archetypes/archetypes.json"

if not os.path.exists(archetypes_file):
    print("Archetypes file does not exist: %s" % archetypes_file)
    exit(1)

f = open(archetypes_file)
data = json.load(f)

print(".. _archetypes:")
print()
print("Archetypes")
print("----------")
print()
print("Below is a table with all the archetypes in Gaia Sky. For each archetype, we list its parent (if any) and its :ref:`components`.")
print()
print("A general description of archetypes and components is provided in :ref:`data-morphology`.")
print()

archetypes = data['archetypes']
arch_str = ""
n = len(archetypes)
for i, key in enumerate(archetypes):
    if i > 0:
        arch_str += ", "
    arch_str += ":ref:`" + key + " <arch-" + key + ">`"

print()
print("All archetypes are: %s." % arch_str)
print()
print(".. list-table:: Archetypes table")
print("   :header-rows: 1")
print("")
print("   * - **Archetype**")
print("     - **Parent**")
print("     - **Components**")

for key in archetypes:
    name = key
    archetype = archetypes[key]
    print()
    print("       .. _arch-%s:" % name)
    print("   * - **%s**" % name)
    
    # Parent
    if archetype['parent'] is not None:
        print("     - :ref:`%s <arch-%s>`" % (archetype['parent'], archetype['parent']))
    else:
        print("     - ")

    components = archetype['components']
    for i, component_dirty in enumerate(components):
        if i == 0:
            print("     - |", end=" ")
        else:
            print("       |", end=" ")

        in_map = "*" not in component_dirty
        component = component_dirty.replace('*', '')

        if in_map:
            print(":ref:`%s <comp-%s>`" % (component, component))
        else:
            print("%s" % component)

print()
print(".. include:: ../global.rst")

f.close()
